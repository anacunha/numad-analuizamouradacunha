package edu.neu.madcourse.analuizamouradacunha.wordgame;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;

import edu.neu.madcourse.analuizamouradacunha.R;

public class WordGame extends Activity implements OnClickListener {

    private static final int CONT = 2668;
    private static final String CONTINUE = "CONTINUE";
    private static final String NEW_GAME = "NEW_GAME";
    private static final String PREFS = "PREFS";
    private View resumeGameButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wordgame);

        View newGameButton = findViewById(R.id.new_game_button);
        newGameButton.setOnClickListener(this);
        resumeGameButton = findViewById(R.id.resume_game_button);
        resumeGameButton.setOnClickListener(this);
        View instructionsButton = findViewById(R.id.instructions_button);
        instructionsButton.setOnClickListener(this);
        View acknowledgementsButton = findViewById(R.id.acknowledgements_button);
        acknowledgementsButton.setOnClickListener(this);
        View quitButton = findViewById(R.id.quit_button);
        quitButton.setOnClickListener(this);

        if(getSharedPreferences(PREFS, MODE_PRIVATE).getBoolean(CONTINUE, false))
            resumeGameButton.setVisibility(View.VISIBLE);
    }

    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.new_game_button:
                intent = new Intent(this, Game.class);
                intent.putExtra(NEW_GAME, true);
                startActivityForResult(intent, CONT);
                break;
            case R.id.resume_game_button:
                intent = new Intent(this, Game.class);
                startActivityForResult(intent, CONT);
                break;
            case R.id.instructions_button:
                intent = new Intent(this, Instructions.class);
                startActivity(intent);
                break;
            case R.id.acknowledgements_button:
                intent = new Intent(this, Acknowledgements.class);
                startActivity(intent);
                break;
            case R.id.quit_button:
                finish();
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                startActivity(new Intent(this, Prefs.class));
                return true;
            // More items go here (if any) ...
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == CONT) {
            if (resultCode == RESULT_OK) {
                // Game was stopped
                // Show Resume Game button
                resumeGameButton.setVisibility(View.VISIBLE);
            }
            else if (resultCode == RESULT_CANCELED) {
                // Game ended
                // Hide Resume Game button
                resumeGameButton.setVisibility(View.GONE);
            }
        }
    }
}
