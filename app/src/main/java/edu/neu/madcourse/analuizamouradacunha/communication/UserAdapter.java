package edu.neu.madcourse.analuizamouradacunha.communication;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import edu.neu.madcourse.analuizamouradacunha.R;

public class UserAdapter extends ArrayAdapter<User> {

    Context mContext;
    int layoutResourceId;
    ArrayList<User> data;

    public UserAdapter(Context mContext, int layoutResourceId, ArrayList<User> data) {

        super(mContext, layoutResourceId, data);

        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            // inflate the layout
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(layoutResourceId, parent, false);
        }

        // object item based on the position
        User user = data.get(position);

        // get the TextView and then set the text (item name) and tag (item ID) values
        TextView textViewItem = (TextView) convertView.findViewById(android.R.id.text1);
        textViewItem.setText(user.getUserName());
        textViewItem.setTag(user.getParseObject());

        return convertView;
    }

}