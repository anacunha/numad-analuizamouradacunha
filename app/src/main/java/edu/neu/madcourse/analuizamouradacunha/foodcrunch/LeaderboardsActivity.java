package edu.neu.madcourse.analuizamouradacunha.foodcrunch;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.parse.CountCallback;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import edu.neu.madcourse.analuizamouradacunha.R;

public class LeaderboardsActivity extends Activity {

    private static final String PHONE_NUMBER = "PHONE_NUMBER";
    private static final String PHONE_NUMBER_DB = "PHONE_NUMBER_DB";
    private static final String PHOTO = "PHOTO";
    private static final String NUMBER = "number";
    private static final String LOG_TAG = "Food Crunch";
    private LinearLayout mProgressLayout;
    private LinearLayout mLeaderboardTitleLayout;
    private ListView mLeaderboardsListView;
    private ArrayList<LeaderboardEntry> leaderboard;
    private String mUserPhoneNumber;
    private int totalUsers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.food_crunch_leaderboards);
        setTitle("Leaderboards");

        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        mUserPhoneNumber = telephonyManager.getLine1Number();

        mProgressLayout = (LinearLayout) findViewById(R.id.progressLayout);
        mLeaderboardTitleLayout = (LinearLayout) findViewById(R.id.leaderboards_title);
        mLeaderboardsListView = (ListView) findViewById(R.id.leaderboards_list_view);

        mProgressLayout.setVisibility(View.VISIBLE);
        mLeaderboardTitleLayout.setVisibility(View.INVISIBLE);
        mLeaderboardsListView.setVisibility(View.INVISIBLE);

        leaderboard = new ArrayList<>();
        getLeaderboard();
    }

    private void displayLeaderboard() {
        // Sort leaderboards before displaying them
        Collections.sort(leaderboard);

        for (LeaderboardEntry entry : leaderboard) {

            if (entry.getUserPhoneNumber().equals(mUserPhoneNumber)) {
                // This is the current user
                // Display it differently ???
                Log.d(LOG_TAG, "You have " + entry.getUserPins() + " pins");
            }

            // Every leaderboard entry has a phone number
            // and the number of pins that user (the owner of the phone number) has
            Log.d(LOG_TAG, entry.getUserPhoneNumber() + " has " + entry.getUserPins() + " pins");
        }

        LeaderboardEntry[] leaderboardEntries = new LeaderboardEntry[leaderboard.size()];

        for (int i = 0; i < leaderboard.size(); i++)
            leaderboardEntries[i] = leaderboard.get(i);

        ListAdapter listAdapter = new LeaderboardsAdapter(getApplicationContext(), leaderboardEntries);
        ListView listView = (ListView)findViewById(R.id.leaderboards_list_view);
        listView.setAdapter(listAdapter);

        mProgressLayout.setVisibility(View.GONE);
        mLeaderboardTitleLayout.setVisibility(View.VISIBLE);
        mLeaderboardsListView.setVisibility(View.VISIBLE);
    }

    private void getLeaderboard() {
        // Get every Food Crunch user
        ParseQuery<ParseObject> query = ParseQuery.getQuery(PHONE_NUMBER_DB);
        query.findInBackground(new FindCallback<ParseObject>() {

            @Override
            public void done(final List<ParseObject> users, ParseException e) {
                if (e == null) {
                    for(int i = 0; i < users.size(); i++) {
                        final String phoneNumber = users.get(i).getString(NUMBER);
                        totalUsers = users.size();

                        // Count number of pins for that user
                        ParseQuery<ParseObject> query = ParseQuery.getQuery(PHOTO);
                        query.whereEqualTo(PHONE_NUMBER, phoneNumber);
                        query.countInBackground(new CountCallback() {
                            @Override
                            public void done(int count, ParseException e) {
                                if (e == null) {
                                    LeaderboardEntry entry = new LeaderboardEntry(phoneNumber, count);
                                    leaderboard.add(entry);

                                    // Only display leaderboard once we're done loading entries
                                    if(leaderboard.size() == totalUsers)
                                        displayLeaderboard();
                                }
                                else {
                                    Log.d(LOG_TAG, "Error: " + e.getMessage());
                                }
                            }
                        });
                    }
                }
                else {
                    Log.d(LOG_TAG, "Error: " + e.getMessage());
                }
            }
        });
    }

    private class LeaderboardsAdapter extends ArrayAdapter<LeaderboardEntry> {
        public LeaderboardsAdapter(Context context, LeaderboardEntry[] set) {
            super(context, R.layout.food_crunch_leaderboards_adapter, set);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            View customView = layoutInflater.inflate(R.layout.food_crunch_leaderboards_adapter, parent, false);

            LeaderboardEntry leaderboardEntry = getItem(position);
            TextView textView = (TextView)customView.findViewById(R.id.leaderboards_index_number);
            TextView textView2 = (TextView)customView.findViewById(R.id.leaderboards_user_name);
            TextView textView3 = (TextView)customView.findViewById(R.id.leaderboards_pin_number);

            textView.setText(String.valueOf(position + 1));
            if (leaderboardEntry.getUserPhoneNumber().equals(mUserPhoneNumber)){
                textView2.setText("You");
            }
            else{
                textView2.setText("Food Crunch User");
            }
            textView3.setText(String.valueOf(leaderboardEntry.getUserPins()));

            return customView;
        }
    }
}
