package edu.neu.madcourse.analuizamouradacunha.foodcrunch;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import edu.neu.madcourse.analuizamouradacunha.R;

public class AchievementsActivity extends Activity {

    private final String[] achievementHeaders = new String[5];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.food_crunch_achievements);
        setTitle("Achievements");

        achievementHeaders[0] = "Got FoodCrunch!";
        achievementHeaders[1] = "10 Pins!";
        achievementHeaders[2] = "100 Pins!";
        achievementHeaders[3] = "Got 10 Likes!";
        achievementHeaders[4] = "Your First Comment!";

        String[] example = {"Congratulations on getting FoodCrunch! You're on your way :)",
                "You got your first 10 pins, keep it going!",
                "Incredible! You pinned 100 times!",
                "Seems like you're doing something right! Got 10 likes on your food board",
                "Someone commented on your food board! Make sure to give back encouragement",};

        ListAdapter listAdapter = new AchievementsAdapter(getApplicationContext(), example);
        ListView listView = (ListView)findViewById(R.id.achievements_list_view);
        listView.setAdapter(listAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });
    }

    private class AchievementsAdapter extends ArrayAdapter<String> {
        public AchievementsAdapter(Context context, String[] texts) {
            super(context, R.layout.food_crunch_achievements_adapter, texts);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            View customView = layoutInflater.inflate(R.layout.food_crunch_achievements_adapter, parent, false);

            String achievementText = getItem(position);
            TextView textView = (TextView)customView.findViewById(R.id.achievements_text);
            TextView textView2 = (TextView)customView.findViewById(R.id.achievement_header);
            ImageView imageView = (ImageView)customView.findViewById(R.id.achievements_image);

            textView.setText(achievementText);
            textView2.setText(achievementHeaders[position]);

            if (!MainMenuActivity.mPref.getBoolean("achievement"+position, false)) {
                imageView.setImageResource(R.drawable.lock);
                textView.setTextColor(Color.parseColor("#999999"));
                textView2.setTextColor(Color.parseColor("#999999"));
            }
            else {
                imageView.setImageResource(R.drawable.checked);
            }
            return customView;
        }
    }

}
