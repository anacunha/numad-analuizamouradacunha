package edu.neu.madcourse.analuizamouradacunha.communication;

import android.app.Activity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

import edu.neu.madcourse.analuizamouradacunha.R;


public class Acknowledgements extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.communication_acknowledgements);

        TextView authorLink = (TextView) findViewById(R.id.gcm_credits);
        authorLink.setMovementMethod(LinkMovementMethod.getInstance());

        TextView authorLink2 = (TextView) findViewById(R.id.parse_credits);
        authorLink2.setMovementMethod(LinkMovementMethod.getInstance());
    }
}
