package edu.neu.madcourse.analuizamouradacunha.foodcrunch;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transfermanager.Download;
import com.amazonaws.mobileconnectors.s3.transfermanager.TransferManager;
import com.amazonaws.regions.Regions;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import edu.neu.madcourse.analuizamouradacunha.R;

public class FriendsBoardActivity extends Activity implements View.OnClickListener {

    private static final String BUCKET_NAME = "foodcrunch";
    private static final String FRIEND = "FRIEND";
    private static final String FRIEND_NAME = "FRIEND_NAME";
    private static final String FRIEND_PHONE_NUMBER = "FRIEND_PHONE_NUMBER";
    private static final String PHOTO_PATH = "PHOTO_PATH";
    private static final String PHOTO_ID = "PHOTO_ID";
    private static final String POSITION = "POSITION";
    private static final String LOG_TAG = "Food Crunch";
    private static final int REQUEST_VIEW_PHOTO = 3;
    private Calendar calendar;
    private File storageDir;
    private TableLayout mGridLayout;
    private LinearLayout mProgressLayout;
    private LinearLayout mButtonsLayout;
    private ImageView mImageView;
    private TextView mMonth;
    private TextView mMonday;
    private TextView mTuesday;
    private TextView mWednesday;
    private TextView mThursday;
    private TextView mFriday;
    private TextView mSaturday;
    private TextView mSunday;
    private String mCurrentPhotoPath;
    private String mFriendPhoneNumber;
    private String[] grid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.food_crunch_friend_board);
        setTitle(getIntent().getStringExtra(FRIEND_NAME) + " Board");

        grid = new String[42];

        mGridLayout = (TableLayout) findViewById(R.id.gridLayout);
        mButtonsLayout = (LinearLayout) findViewById(R.id.buttonsLayout);
        mProgressLayout = (LinearLayout) findViewById(R.id.progressLayout);
        mMonth = (TextView) findViewById(R.id.month);
        mMonday = (TextView) findViewById(R.id.monday_day);
        mTuesday = (TextView) findViewById(R.id.tuesday_day);
        mWednesday = (TextView) findViewById(R.id.wednesday_day);
        mThursday = (TextView) findViewById(R.id.thursday_day);
        mFriday = (TextView) findViewById(R.id.friday_day);
        mSaturday = (TextView) findViewById(R.id.saturday_day);
        mSunday = (TextView) findViewById(R.id.sunday_day);

        storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        mFriendPhoneNumber = getIntent().getStringExtra(FRIEND_PHONE_NUMBER);

        calendar = Calendar.getInstance();
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        setCurrentWeek();
    }

    private void setCurrentWeek() {
        mMonth.setText(getCurrentMonth(calendar.get(Calendar.MONTH)));

        // Set the calendar to monday of the current week
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

        // Print dates of the current week starting on Monday
        DateFormat day = new SimpleDateFormat("dd", Locale.getDefault());
        DateFormat date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

        mMonday.setText(day.format(calendar.getTime()));
        mMonday.setTag(date.format(calendar.getTime()));
        calendar.add(Calendar.DATE, 1);
        mTuesday.setText(day.format(calendar.getTime()));
        mTuesday.setTag(date.format(calendar.getTime()));
        calendar.add(Calendar.DATE, 1);
        mWednesday.setText(day.format(calendar.getTime()));
        mWednesday.setTag(date.format(calendar.getTime()));
        calendar.add(Calendar.DATE, 1);
        mThursday.setText(day.format(calendar.getTime()));
        mThursday.setTag(date.format(calendar.getTime()));
        calendar.add(Calendar.DATE, 1);
        mFriday.setText(day.format(calendar.getTime()));
        mFriday.setTag(date.format(calendar.getTime()));
        calendar.add(Calendar.DATE, 1);
        mSaturday.setText(day.format(calendar.getTime()));
        mSaturday.setTag(date.format(calendar.getTime()));
        calendar.add(Calendar.DATE, 1);
        mSunday.setText(day.format(calendar.getTime()));
        mSunday.setTag(date.format(calendar.getTime()));

        new DownloadFilesTask().execute();
    }

    private String getCurrentMonth(int monthIndex) {
        String[] months = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
        return months[monthIndex];
    }

    public void viewPhoto(View v) {
        mImageView = (ImageView) v;
        //dispatchTakePictureIntent(Integer.valueOf((String) v.getTag()));
    }

    private void setPic() {
        // Get the dimensions of the View
        int targetW = mImageView.getWidth();
        int targetH = mImageView.getHeight();
        mImageView.setMaxWidth(targetW);
        mImageView.setMaxHeight(targetH);

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        mImageView.setImageBitmap(bitmap);
        mImageView.setOnClickListener(this);

        int imagePosition = Integer.valueOf((String) mImageView.getTag());
        checkWeekDay(imagePosition);

        Log.d(LOG_TAG, "Saved grid position " + imagePosition);
        Log.d(LOG_TAG, "Photo Path: " + mCurrentPhotoPath);
        grid[imagePosition] = mCurrentPhotoPath;
    }

    private void checkWeekDay(int imagePosition) {
        String weekDay = getWeekDay(imagePosition);
        int checkMarkId = getResources().getIdentifier(weekDay + "_check", "id", getPackageName());
        ImageView mCheckMark = (ImageView) findViewById(checkMarkId);
        mCheckMark.setImageResource(R.drawable.checked);
    }

    private String getDate(int imagePosition) {
        String date = "";
        switch (imagePosition % 7) {
            case 0:
                date = (String) mMonday.getTag();
                break;
            case 1:
                date = (String) mTuesday.getTag();
                break;
            case 2:
                date = (String) mWednesday.getTag();
                break;
            case 3:
                date = (String) mThursday.getTag();
                break;
            case 4:
                date = (String) mFriday.getTag();
                break;
            case 5:
                date = (String) mSaturday.getTag();
                break;
            case 6:
                date = (String) mSunday.getTag();
                break;
        }

        return date;
    }

    private String getWeekDay(int imagePosition) {
        String weekDay = "";
        switch (imagePosition % 7) {
            case 0:
                weekDay = "monday";
                break;
            case 1:
                weekDay = "tuesday";
                break;
            case 2:
                weekDay = "wednesday";
                break;
            case 3:
                weekDay = "thursday";
                break;
            case 4:
                weekDay = "friday";
                break;
            case 5:
                weekDay = "saturday";
                break;
            case 6:
                weekDay = "sunday";
                break;
        }

        return weekDay;
    }

    @Override
    public void onClick(View v) {
        ImageView view = (ImageView) v;
        int imagePosition = Integer.valueOf((String) view.getTag());

        // If the path for that ImageView is not empty,
        // then there's a photo on that grid
        if (grid[imagePosition] != null) {
            Intent viewPinIntent = new Intent(this, ViewPhotoActivity.class);
            viewPinIntent.putExtra(POSITION, imagePosition);
            viewPinIntent.putExtra(PHOTO_PATH, grid[imagePosition]);
            viewPinIntent.putExtra(PHOTO_ID, view.getId());
            viewPinIntent.putExtra(FRIEND, true);
            startActivity(viewPinIntent);
        }
    }

    private void clearGrid() {
        grid = new String[42];
        ((ImageView) findViewById(R.id.monday_check)).setImageResource(R.drawable.unchecked);
        ((ImageView) findViewById(R.id.tuesday_check)).setImageResource(R.drawable.unchecked);
        ((ImageView) findViewById(R.id.wednesday_check)).setImageResource(R.drawable.unchecked);
        ((ImageView) findViewById(R.id.thursday_check)).setImageResource(R.drawable.unchecked);
        ((ImageView) findViewById(R.id.friday_check)).setImageResource(R.drawable.unchecked);
        ((ImageView) findViewById(R.id.saturday_check)).setImageResource(R.drawable.unchecked);
        ((ImageView) findViewById(R.id.sunday_check)).setImageResource(R.drawable.unchecked);
    }

    public void updateWeek(View v) {
        String tag = (String) v.getTag();

        clearGrid();
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

        if(tag.equals("previous")) {
            // Set the calendar to monday of the previous week
            calendar.add(Calendar.DATE, -7);
        }
        else {
            // Set the calendar to monday of the next week
            calendar.add(Calendar.DATE, 7);
        }

        // Print dates of the current week starting on Monday
        DateFormat day = new SimpleDateFormat("dd", Locale.getDefault());
        DateFormat date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

        mMonth.setText(getCurrentMonth(calendar.get(Calendar.MONTH)));

        mMonday.setText(day.format(calendar.getTime()));
        mMonday.setTag(date.format(calendar.getTime()));
        calendar.add(Calendar.DATE, 1);
        mTuesday.setText(day.format(calendar.getTime()));
        mTuesday.setTag(date.format(calendar.getTime()));
        calendar.add(Calendar.DATE, 1);
        mWednesday.setText(day.format(calendar.getTime()));
        mWednesday.setTag(date.format(calendar.getTime()));
        calendar.add(Calendar.DATE, 1);
        mThursday.setText(day.format(calendar.getTime()));
        mThursday.setTag(date.format(calendar.getTime()));
        calendar.add(Calendar.DATE, 1);
        mFriday.setText(day.format(calendar.getTime()));
        mFriday.setTag(date.format(calendar.getTime()));
        calendar.add(Calendar.DATE, 1);
        mSaturday.setText(day.format(calendar.getTime()));
        mSaturday.setTag(date.format(calendar.getTime()));
        calendar.add(Calendar.DATE, 1);
        mSunday.setText(day.format(calendar.getTime()));
        mSunday.setTag(date.format(calendar.getTime()));

        new DownloadFilesTask().execute();
    }

    private class DownloadFilesTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            CognitoCachingCredentialsProvider cognitoProvider = new CognitoCachingCredentialsProvider(
                    FriendsBoardActivity.this.getApplication(),
                    "us-east-1:a87ab379-f522-435c-baab-eacb5e44e04f",
                    Regions.US_EAST_1
            );

            TransferManager transferManager = new TransferManager(cognitoProvider);

            for(int i = 0; i < 42; i++) {
                String fileName = mFriendPhoneNumber + "_" + getDate(i) + "_" + i + ".jpg";
                File f = new File(storageDir + "/" + fileName);
                mCurrentPhotoPath = f.getAbsolutePath();

                try {
                    Download download = transferManager.download(BUCKET_NAME, fileName, f);
                    download.waitForCompletion();
                    grid[i] = mCurrentPhotoPath;
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            mProgressLayout.setVisibility(View.VISIBLE);
            mGridLayout.setVisibility(View.INVISIBLE);
            mButtonsLayout.setVisibility(View.INVISIBLE);
        }

        @Override
        protected void onPostExecute(Void result) {
            mProgressLayout.setVisibility(View.GONE);
            mGridLayout.setVisibility(View.VISIBLE);
            mButtonsLayout.setVisibility(View.VISIBLE);
            updateGrid();
        }
    }

    private void updateGrid() {
        for(int i = 0; i < 42; i++) {
            mImageView = (ImageView) findViewById(getResources().getIdentifier("grid_" + i, "id", getPackageName()));
            mCurrentPhotoPath = grid[i];
            if(mCurrentPhotoPath != null)
                setPic();
            else
                mImageView.setImageResource(R.drawable.grid);
        }
    }
}
