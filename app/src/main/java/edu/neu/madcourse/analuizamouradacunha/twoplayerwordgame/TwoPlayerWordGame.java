package edu.neu.madcourse.analuizamouradacunha.twoplayerwordgame;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

import edu.neu.madcourse.analuizamouradacunha.Main;
import edu.neu.madcourse.analuizamouradacunha.R;
import edu.neu.madcourse.analuizamouradacunha.communication.Communication;
import edu.neu.madcourse.analuizamouradacunha.communication.Constants;
import edu.neu.madcourse.analuizamouradacunha.communication.HighScores;
import edu.neu.madcourse.analuizamouradacunha.wordgame.Acknowledgements;
import edu.neu.madcourse.analuizamouradacunha.wordgame.Instructions;
import edu.neu.madcourse.analuizamouradacunha.wordgame.Prefs;

public class TwoPlayerWordGame extends Activity implements OnClickListener {

    private static final int REGS = 7347;
    private static final int CONT = 2668;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "Two Player WordFade";
    private static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private View mResumeMatch;
    private View mChallengePlayer;
    private String registrationID;
    private Context context;
    private String currentUserObjectID;
    private String matchID;
    private String playerOneID;
    private String playerTwoID;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.two_player_wordgame);

        mChallengePlayer = findViewById(R.id.challenge_player_button);
        mChallengePlayer.setOnClickListener(this);
        mResumeMatch = findViewById(R.id.resume_match_button);
        mResumeMatch.setOnClickListener(this);
        View mHighScores = findViewById(R.id.high_scores_button);
        mHighScores.setOnClickListener(this);
        View mInstructions = findViewById(R.id.instructions_button);
        mInstructions.setOnClickListener(this);
        View mAcknowledgements = findViewById(R.id.acknowledgements_button);
        mAcknowledgements.setOnClickListener(this);
        View mQuit = findViewById(R.id.quit_button);
        mQuit.setOnClickListener(this);

        context = getApplicationContext();
    }

    @Override
    public void onResume() {
        super.onResume();
        checkRegistration();
    }

    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.challenge_player_button:
                intent = new Intent(this, ChallengePlayer.class);
                startActivity(intent);
                break;
            case R.id.resume_match_button:
                // Go to current match
                intent = new Intent(this, Match.class);
                intent.putExtra(Constants.MATCH, matchID);
                intent.putExtra(Constants.PLAYER_ONE, playerOneID);
                intent.putExtra(Constants.PLAYER_TWO, playerTwoID);
                startActivityForResult(intent, CONT);
                break;
            case R.id.high_scores_button:
                intent = new Intent(this, HighScores.class);
                startActivity(intent);
                break;
            case R.id.instructions_button:
                intent = new Intent(this, Instructions.class);
                startActivity(intent);
                break;
            case R.id.acknowledgements_button:
                intent = new Intent(this, Acknowledgements.class);
                startActivity(intent);
                break;
            case R.id.quit_button:
                //startActivity(new Intent(this, Main.class));
                finish();
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                startActivity(new Intent(this, Prefs.class));
                return true;
            // More items go here (if any) ...
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == REGS) {
            if (resultCode == RESULT_OK) {
                registrationID = getRegistrationId(context);
                if (TextUtils.isEmpty(registrationID)) {
                    Toast.makeText(this, "Could not register for online play", Toast.LENGTH_LONG).show();
                    //startActivity(new Intent(this, Main.class));
                    finish();
                }
            } else if (resultCode == RESULT_CANCELED) {
                // User was not registered
                Toast.makeText(this, "Could not register for online play", Toast.LENGTH_LONG).show();
                //startActivity(new Intent(this, Main.class));
                finish();
            }
        } else if (requestCode == CONT) {
            // User has abandoned the match
            if (resultCode == RESULT_CANCELED) {
                mResumeMatch.setVisibility(View.GONE);
                mChallengePlayer.setVisibility(View.VISIBLE);
            }
        }
    }

    private void checkRegistration() {
        if (checkPlayServices()) {
            registrationID = getRegistrationId(context);
            if (TextUtils.isEmpty(registrationID)) {
                // If user is not registered, we open up the registration activity
                Intent intent = new Intent(this, Registration.class);
                startActivityForResult(intent, REGS);
            } else
                getUserInfo();
        }
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                //startActivity(new Intent(this, Main.class));
                finish();
            }
            return false;
        }
        return true;
    }

    @SuppressLint("NewApi")
    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION,
                Integer.MIN_VALUE);
        Log.i(TAG, String.valueOf(registeredVersion));
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }

    private SharedPreferences getGCMPreferences(Context context) {
        return getSharedPreferences(Communication.class.getSimpleName(), Context.MODE_PRIVATE);
    }

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    private void isUserInMatch() {
        if (currentUserObjectID != null && !currentUserObjectID.isEmpty()) {
            ParseQuery<ParseObject> q1 = ParseQuery.getQuery(Constants.MATCH);
            q1.whereEqualTo(Constants.PLAYER_ONE, ParseObject.createWithoutData(Constants.USER, currentUserObjectID));
            q1.whereDoesNotExist(Constants.WINNER);
            ParseQuery<ParseObject> q2 = ParseQuery.getQuery(Constants.MATCH);
            q2.whereEqualTo(Constants.PLAYER_TWO, ParseObject.createWithoutData(Constants.USER, currentUserObjectID));
            q2.whereDoesNotExist(Constants.WINNER);

            List<ParseQuery<ParseObject>> queries = new ArrayList<>();
            queries.add(q1);
            queries.add(q2);

            ParseQuery<ParseObject> query = ParseQuery.or(queries);
            query.getFirstInBackground(new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject match, ParseException e) {
                    if (e == null) {
                        if (match != null) {
                            // Current Player is on an ongoing match
                            mResumeMatch.setVisibility(View.VISIBLE);
                            mChallengePlayer.setVisibility(View.GONE);
                            matchID = match.getObjectId();
                            playerOneID = match.getParseObject(Constants.PLAYER_ONE).getObjectId();
                            playerTwoID = match.getParseObject(Constants.PLAYER_TWO).getObjectId();
                        }
                    } else {
                        Log.d(TAG, "Error: " + e.getMessage());
                    }
                }
            });
        }
    }

    private void getUserInfo() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery(Constants.USER);
        query.whereEqualTo(Constants.REGISTRATION_ID, registrationID);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
                if (e == null) {
                    for (ParseObject user : parseObjects) {
                        currentUserObjectID = user.getObjectId();
                        Log.d(TAG, "Identified User: " + user.getString(Constants.USER_NAME));
                        isUserInMatch();
                    }
                } else
                    Log.d(TAG, "Error: " + e.getMessage());
            }
        });
    }
}
