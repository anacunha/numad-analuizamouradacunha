package edu.neu.madcourse.analuizamouradacunha.dictionary;

import android.app.Activity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import edu.neu.madcourse.analuizamouradacunha.R;

public class Acknowledgements extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dictionary_acknowledgements);
        setTitle("Dictionary Acknowledgements");

        TextView soundBible = (TextView) findViewById(R.id.sound_bible);
        soundBible.setMovementMethod(LinkMovementMethod.getInstance());

        TextView databaseCredit = (TextView) findViewById(R.id.database_credit);
        databaseCredit.setMovementMethod(LinkMovementMethod.getInstance());

        TextView asyncTaskCredit = (TextView) findViewById(R.id.async_task_credit);
        asyncTaskCredit.setMovementMethod(LinkMovementMethod.getInstance());
    }
}
