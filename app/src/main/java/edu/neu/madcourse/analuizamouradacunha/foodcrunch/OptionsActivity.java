package edu.neu.madcourse.analuizamouradacunha.foodcrunch;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;

import edu.neu.madcourse.analuizamouradacunha.R;

public class OptionsActivity extends Activity {
    public static String mealTimeNotification = "MealTimeNotification";
    public static CheckBox mCheckBox;
    public static Button mBreakfastButton;
    public static TextView mBreakfastTextView;
    public static Button mLunchButton;
    public static TextView mLunchTextView;
    public static Button mDinnerButton;
    public static TextView mDinnerTextView;
    public static TextView mSetMealTimesTextView;
    public static boolean breakfastFlag = false;
    public static boolean lunchFlag = false;
    public static boolean dinnerFlag = false;
    public static Context context;
    public static PendingIntent breakfastPendingIntent;
    public static Intent breakfastIntent;
    public static PendingIntent lunchPendingIntent;
    public static Intent lunchIntent;
    public static PendingIntent dinnerPendingIntent;
    public static Intent dinnerIntent;
    public static AlarmManager breakfastAlarmManager;
    public static AlarmManager lunchAlarmManager;
    public static AlarmManager dinnerAlarmManager;
    NotificationManager notificationManager;
    boolean isNotificActive = false;
    int notifID = 33;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.food_crunch_options_menu);
        setTitle("Options");

        mCheckBox = (CheckBox)findViewById(R.id.options_menu_checkbox);
        mSetMealTimesTextView = (TextView)findViewById(R.id.set_meal_times);
        mBreakfastButton = (Button)findViewById(R.id.breakfast_button);
        mLunchButton = (Button)findViewById(R.id.lunch_button);
        mDinnerButton = (Button)findViewById(R.id.dinner_button);
        mBreakfastTextView = (TextView)findViewById(R.id.breakfast_text);
        mLunchTextView = (TextView)findViewById(R.id.lunch_text);
        mDinnerTextView = (TextView)findViewById(R.id.dinner_text);
        context = getApplicationContext();

        if (!mCheckBox.isChecked()){
            mSetMealTimesTextView.setVisibility(View.INVISIBLE);
            mBreakfastButton.setVisibility(View.INVISIBLE);
            mLunchButton.setVisibility(View.INVISIBLE);
            mDinnerButton.setVisibility(View.INVISIBLE);
            mBreakfastTextView.setVisibility(View.INVISIBLE);
            mLunchTextView.setVisibility(View.INVISIBLE);
            mDinnerTextView.setVisibility(View.INVISIBLE);
        }
        mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (buttonView.isChecked()){
                    mSetMealTimesTextView.setVisibility(View.VISIBLE);
                    mBreakfastButton.setVisibility(View.VISIBLE);
                    mLunchButton.setVisibility(View.VISIBLE);
                    mDinnerButton.setVisibility(View.VISIBLE);
                    mBreakfastTextView.setVisibility(View.VISIBLE);
                    mLunchTextView.setVisibility(View.VISIBLE);
                    mDinnerTextView.setVisibility(View.VISIBLE);
                }
                else{
                    mSetMealTimesTextView.setVisibility(View.INVISIBLE);
                    mBreakfastButton.setVisibility(View.INVISIBLE);
                    mLunchButton.setVisibility(View.INVISIBLE);
                    mDinnerButton.setVisibility(View.INVISIBLE);
                    mBreakfastTextView.setVisibility(View.INVISIBLE);
                    mLunchTextView.setVisibility(View.INVISIBLE);
                    mDinnerTextView.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    public void showBreakfastTimePicker(View v){
        breakfastFlag = true;
        lunchFlag = false;
        dinnerFlag = false;
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getFragmentManager(), "timePicker");

    }

    public void showLunchTimePicker(View v){
        lunchFlag = true;
        breakfastFlag = false;
        dinnerFlag = false;
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getFragmentManager(), "timePicker");

    }

    public void showDinnerTimePicker(View v){
        dinnerFlag = true;
        lunchFlag = false;
        breakfastFlag = false;
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getFragmentManager(), "timePicker");

    }

    public static void setBreakfastNotification(int hourOfDay, int minute){
        final int _id = (int) System.currentTimeMillis();

        breakfastAlarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        breakfastIntent = new Intent(context, AlarmReceiver.class);
        breakfastIntent.putExtra(mealTimeNotification, 1);
        breakfastPendingIntent = PendingIntent.getBroadcast(context, _id, breakfastIntent, 0);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minute);
        breakfastAlarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                AlarmManager.INTERVAL_DAY, breakfastPendingIntent);


    }

    public static void setLunchNotification(int hourOfDay, int minute){
        final int _id = (int) System.currentTimeMillis();
        lunchAlarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        lunchIntent = new Intent(context, AlarmReceiver.class);
        lunchIntent.putExtra(mealTimeNotification, 2);
        lunchPendingIntent = PendingIntent.getBroadcast(context, _id, lunchIntent, 0);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minute);
        lunchAlarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                AlarmManager.INTERVAL_DAY, lunchPendingIntent);
    }

    public static void setDinnerNotification(int hourOfDay, int minute){
        final int _id = (int) System.currentTimeMillis();
        dinnerAlarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        dinnerIntent = new Intent(context, AlarmReceiver.class);
        dinnerIntent.putExtra(mealTimeNotification, 3);
        dinnerPendingIntent = PendingIntent.getBroadcast(context, _id, dinnerIntent, 0);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minute);
        dinnerAlarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                AlarmManager.INTERVAL_DAY, dinnerPendingIntent);
    }

    public static class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            //Use the current time as the default values for the picker
            Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            //Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));

        }

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            //Do something with the time chosen by the user
            if (breakfastFlag == true){
                if (hourOfDay == 0){
                    if (minute == 0){
                        mBreakfastTextView.setText("Breakfast: 12:00 AM");
                    }
                    else if (minute > 0 && minute < 10){
                        mBreakfastTextView.setText("Breakfast:  12:0" + minute + " AM");
                    }
                    else{
                        mBreakfastTextView.setText("Breakfast:  12:" + minute + " AM");
                    }
                }
                else if (hourOfDay < 12){
                    if (minute == 0){
                        mBreakfastTextView.setText("Breakfast: " + hourOfDay + ":00 AM");
                    }
                    else if (minute > 0 && minute < 10){
                        mBreakfastTextView.setText("Breakfast: " + hourOfDay + ":0" + minute + " AM");
                    }
                    else{
                        mBreakfastTextView.setText("Breakfast: " + hourOfDay + ":" + minute + " AM");
                    }

                }
                else if (hourOfDay == 12){


                    if (minute == 0){
                        mBreakfastTextView.setText("Breakfast: " + hourOfDay + ":00 PM");
                    }
                    else if (minute > 0 && minute < 10){
                        mBreakfastTextView.setText("Breakfast: " + hourOfDay + ":0" + minute + " PM");
                    }
                    else{
                        mBreakfastTextView.setText("Breakfast: " + hourOfDay + ":" + minute + " PM");
                    }

                }
                else if (hourOfDay > 12){
                    int hour = hourOfDay - 12;

                    if (minute == 0){
                        mBreakfastTextView.setText("Breakfast: " + hour + ":00 PM");
                    }
                    else if (minute > 0 && minute < 10){
                        mBreakfastTextView.setText("Breakfast: " + hour + ":0" + minute + " PM");
                    }
                    else{
                        mBreakfastTextView.setText("Breakfast: " + hour + ":" + minute + " PM");
                    }

                }

                Log.d("hourOfDay", "hourOfDay is " + hourOfDay);
                setBreakfastNotification(hourOfDay, minute);
                breakfastFlag = false;
            }
            else if (lunchFlag == true){
                if (hourOfDay == 0){
                    if (minute == 0){
                        mLunchTextView.setText("Lunch: 12:00 AM");
                    }
                    else if (minute > 0 && minute < 10){
                        mLunchTextView.setText("Lunch:  12:0" + minute + " AM");
                    }
                    else{
                        mLunchTextView.setText("Lunch:  12:" + minute + " AM");
                    }
                }
                else if (hourOfDay < 12){
                    if (minute == 0){
                        mLunchTextView.setText("Lunch: " + hourOfDay + ":00 AM");
                    }
                    else if (minute > 0 && minute < 10){
                        mLunchTextView.setText("Lunch: " + hourOfDay + ":0" + minute + " AM");
                    }
                    else{
                        mLunchTextView.setText("Lunch: " + hourOfDay + ":" + minute + " AM");
                    }

                }
                else if (hourOfDay == 12){


                    if (minute == 0){
                        mLunchTextView.setText("Lunch: " + hourOfDay + ":00 PM");
                    }
                    else if (minute > 0 && minute < 10){
                        mLunchTextView.setText("Lunch: " + hourOfDay + ":0" + minute + " PM");
                    }
                    else{
                        mLunchTextView.setText("Lunch: " + hourOfDay + ":" + minute + " PM");
                    }

                }
                else if (hourOfDay > 12){
                    int hour = hourOfDay - 12;

                    if (minute == 0){
                        mLunchTextView.setText("Lunch: " + hour + ":00 PM");
                    }
                    else if (minute > 0 && minute < 10){
                        mLunchTextView.setText("Lunch: " + hour + ":0" + minute + " PM");
                    }
                    else{
                        mLunchTextView.setText("Lunch: " + hour + ":" + minute + " PM");
                    }

                }
                Log.d("hourOfDay", "hourOfDay is " + hourOfDay);
                setLunchNotification(hourOfDay, minute);
                lunchFlag = false;
            }
            else if (dinnerFlag == true){
                if (hourOfDay == 0){
                    if (minute == 0){
                        mDinnerTextView.setText("Dinner: 12:00 AM");
                    }
                    else if (minute > 0 && minute < 10){
                        mDinnerTextView.setText("Dinner:  12:0" + minute + " AM");
                    }
                    else{
                        mDinnerTextView.setText("Dinner:  12:" + minute + " AM");
                    }
                }
                else if (hourOfDay < 12){
                    if (minute == 0){
                        mDinnerTextView.setText("Dinner: " + hourOfDay + ":00 AM");
                    }
                    else if (minute > 0 && minute < 10){
                        mDinnerTextView.setText("Dinner: " + hourOfDay + ":0" + minute + " AM");
                    }
                    else{
                        mDinnerTextView.setText("Dinner: " + hourOfDay + ":" + minute + " AM");
                    }

                }
                else if (hourOfDay == 12){


                    if (minute == 0){
                        mDinnerTextView.setText("Dinner: " + hourOfDay + ":00 PM");
                    }
                    else if (minute > 0 && minute < 10){
                        mDinnerTextView.setText("Dinner: " + hourOfDay + ":0" + minute + " PM");
                    }
                    else{
                        mDinnerTextView.setText("Dinner: " + hourOfDay + ":" + minute + " PM");
                    }

                }
                else if (hourOfDay > 12){
                    int hour = hourOfDay - 12;

                    if (minute == 0){
                        mDinnerTextView.setText("Dinner: " + hour + ":00 PM");
                    }
                    else if (minute > 0 && minute < 10){
                        mDinnerTextView.setText("Dinner: " + hour + ":0" + minute + " PM");
                    }
                    else{
                        mDinnerTextView.setText("Dinner: " + hour + ":" + minute + " PM");
                    }

                }
                Log.d("hourOfDay", "hourOfDay is " + hourOfDay);
                setDinnerNotification(hourOfDay, minute);
                dinnerFlag = false;
            }

            this.dismiss();
        }
    }




}
