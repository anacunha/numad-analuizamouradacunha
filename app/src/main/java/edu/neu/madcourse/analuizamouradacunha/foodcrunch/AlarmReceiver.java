package edu.neu.madcourse.analuizamouradacunha.foodcrunch;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import edu.neu.madcourse.analuizamouradacunha.R;

/**
 * Created by michaelnwani on 4/21/15.
 */
public class AlarmReceiver extends BroadcastReceiver {
    public static NotificationManager notificationManager;
    public static NotificationCompat.Builder mBreakfastReminder;
    public static NotificationCompat.Builder mLunchReminder;
    public static NotificationCompat.Builder mDinnerReminder;
    public static PendingIntent mBreakfastPendingIntent;
    public static PendingIntent mLunchPendingIntent;
    public static PendingIntent mDinnerPendingIntent;

    @Override
    public void onReceive(Context context, Intent intent) {

        notificationManager =
                (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        mBreakfastPendingIntent = PendingIntent.getActivity(context, 0,
                new Intent(context, MyBoardActivity.class), 0);
        mLunchPendingIntent = PendingIntent.getActivity(context, 0,
                new Intent(context, MyBoardActivity.class), 0);
        mDinnerPendingIntent = PendingIntent.getActivity(context, 0,
                new Intent(context, MyBoardActivity.class), 0);

        int data = intent.getIntExtra(OptionsActivity.mealTimeNotification, 0);

        Log.d("data", "data is: " + data);
        if (data == 1){
            createBreakfastNotification(context);
        }
        else if (data == 2){
            createLunchNotification(context);
        }
        else if (data == 3){
            createDinnerNotification(context);
        }


    }

    public void createBreakfastNotification(Context context){

        mBreakfastReminder = new
                NotificationCompat.Builder(context)
                .setContentTitle("Breakfast Reminder")
                .setContentText("Remember to pin your breakfast! :)")
                .setTicker("Breakfast Reminder")
                .setSmallIcon(R.drawable.ic_food_crunch);

        mBreakfastReminder.setContentIntent(mBreakfastPendingIntent);
        mBreakfastReminder.setDefaults(NotificationCompat.DEFAULT_SOUND);
        mBreakfastReminder.setAutoCancel(true);
        mBreakfastReminder.setPriority(2);

        notificationManager.notify(10, mBreakfastReminder.build());

    }

    public void createLunchNotification(Context context){
        mLunchReminder = new
                NotificationCompat.Builder(context)
                .setContentTitle("Lunch Reminder")
                .setContentText("Remember to pin your lunch! :)")
                .setTicker("Lunch Reminder")
                .setSmallIcon(R.drawable.ic_food_crunch);

        mLunchReminder.setContentIntent(mLunchPendingIntent);
        mLunchReminder.setDefaults(NotificationCompat.DEFAULT_SOUND);
        mLunchReminder.setAutoCancel(true);
        mLunchReminder.setPriority(2);

        notificationManager.notify(20, mLunchReminder.build());
    }

    public void createDinnerNotification(Context context){
        mDinnerReminder = new
                NotificationCompat.Builder(context)
                .setContentTitle("Dinner Reminder")
                .setContentText("Remember to pin your dinner! :)")
                .setTicker("Dinner Reminder")
                .setSmallIcon(R.drawable.ic_food_crunch);

        mDinnerReminder.setContentIntent(mDinnerPendingIntent);
        mDinnerReminder.setDefaults(NotificationCompat.DEFAULT_SOUND);
        mDinnerReminder.setAutoCancel(true);
        mDinnerReminder.setPriority(2);

        notificationManager.notify(30, mDinnerReminder.build());
    }
}
