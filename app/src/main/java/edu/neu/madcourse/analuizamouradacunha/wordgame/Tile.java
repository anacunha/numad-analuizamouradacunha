package edu.neu.madcourse.analuizamouradacunha.wordgame;

public class Tile {

    private int row;
    private int column;
    private BoardLetter letter;

    public Tile(int row, int column) {
        this.row = row;
        this.column = column;
    }

    public void setLetter(BoardLetter letter) {
        this.letter = letter;
    }

    public BoardLetter getLetter() {
        return letter;
    }

    public int getRow() { return row; }

    public int getColumn() { return column; }
}
