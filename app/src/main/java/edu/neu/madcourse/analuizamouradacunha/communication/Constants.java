package edu.neu.madcourse.analuizamouradacunha.communication;

public final class Constants {

    public static final String USER = "User";
    public static final String USER_NAME = "userName";
    public static final String REGISTRATION_ID = "registrationID";

    public static final String HIGH_SCORE = "HighScore";
    public static final String SCORE = "score";

    public static final String GAME = "Game";
    public static final String BUNCH = "bunch";
    public static final String BOARD = "board";
    public static final String IS_WORD = "isWord";
    public static final String RACK = "rack";
    public static final String ELAPSED_TIME = "elapsedTime";
    public static final String PAUSE = "pause";
    public static final String OVER = "over";

    public static final String NEW_MATCH = "NEW_MATCH";
    public static final String MATCH = "Match";
    public static final String WINNER = "winner";
    public static final String PLAYER_ONE = "playerOne";
    public static final String PLAYER_TWO = "playerTwo";
    public static final String GAME_ONE = "gameOne";
    public static final String GAME_TWO = "gameTwo";
    public static final String OPPONENT = "opponent";
    public static final String PLAYER = "player";
}
