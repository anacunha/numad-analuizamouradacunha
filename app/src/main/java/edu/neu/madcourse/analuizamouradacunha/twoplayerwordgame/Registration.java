package edu.neu.madcourse.analuizamouradacunha.twoplayerwordgame;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.io.IOException;
import java.util.List;

import edu.neu.madcourse.analuizamouradacunha.R;
import edu.neu.madcourse.analuizamouradacunha.communication.Communication;
import edu.neu.madcourse.analuizamouradacunha.communication.CommunicationConstants;

public class Registration extends Activity implements OnClickListener {

    private static final String TAG = "Communication";
    private static final String USER = "User";
    private static final String USER_NAME = "userName";
    private static final String REGISTRATION_ID = "registrationID";
    private EditText mUserNameInput;

    private static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private GoogleCloudMessaging gcm;
    private Context context;
    private String registrationID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.two_player_wordgame_registration);

        mUserNameInput = (EditText) findViewById(R.id.user_name_input);

        gcm = GoogleCloudMessaging.getInstance(this);
        context = getApplicationContext();
    }

    @SuppressLint("NewApi")
    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences();
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION,
                Integer.MIN_VALUE);
        Log.i(TAG, String.valueOf(registeredVersion));
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }

    private SharedPreferences getGCMPreferences() {
        return getSharedPreferences(Communication.class.getSimpleName(), Context.MODE_PRIVATE);
    }

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    private void registerInBackground(final String userName) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    registrationID = gcm.register(CommunicationConstants.GCM_SENDER_ID);
                    storeRegistrationId(context, registrationID);
                } catch (IOException ex) {
                    Log.d(TAG, "Error :" + ex.getMessage());
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                saveUser(userName);
                Toast.makeText(context, "Registration completed", Toast.LENGTH_LONG).show();
                setResult(RESULT_OK, getIntent().putExtra(REGISTRATION_ID, registrationID));
                finish();
            }
        }.execute(null, null, null);
    }

    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGCMPreferences();
        int appVersion = getAppVersion(context);
        Log.i(TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.apply();
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                setResult(RESULT_CANCELED);
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    public void onClick(final View view) {

        switch (view.getId()) {

            case R.id.register_button:
                if (checkPlayServices()) {
                    registrationID = getRegistrationId(context);
                    if (TextUtils.isEmpty(registrationID)) {
                        String userName = mUserNameInput.getText().toString();
                        Log.d(TAG, "Username: " + userName);
                        if (!userName.trim().isEmpty())
                            registerUser(userName);
                    }
                }
                break;

            case R.id.back_button:
                setResult(RESULT_CANCELED);
                finish();
                break;
        }
    }

    private void saveUser(String userName) {
        ParseObject user = new ParseObject(USER);
        user.put(USER_NAME, userName);
        user.put(REGISTRATION_ID,registrationID);
        user.saveInBackground();
    }

    private void registerUser(String userName) {

        final String name = userName;
        ParseQuery<ParseObject> query = ParseQuery.getQuery(USER);
        query.whereEqualTo(USER_NAME, userName);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
                if (e == null) {
                    if (parseObjects.size() == 0) {
                        registerInBackground(name);
                    }
                    else
                        Toast.makeText(context, "Username already in use", Toast.LENGTH_LONG).show();
                }
                else {
                    Log.d(TAG, "Error: " + e.getMessage());
                }
            }
        });
    }
}
