package edu.neu.madcourse.analuizamouradacunha.wordgame;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import edu.neu.madcourse.analuizamouradacunha.R;


public class Instructions extends Activity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wordgame_instructions);

        View backButton = findViewById(R.id.back_button);
        backButton.setOnClickListener(this);
    }

    public void onClick(View v) {
        if (v.getId() == R.id.back_button)
            finish();
    }
}
