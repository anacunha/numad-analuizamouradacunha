package edu.neu.madcourse.analuizamouradacunha;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.PushService;

import edu.neu.madcourse.analuizamouradacunha.foodcrunch.MainMenuActivity;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Parse.initialize(this, "osTQiR7OCaZR1iAOTFZZW7YL2aZppfLrLWehOsXi", "VlHSfHh3cdSbXm9tvtrGEIFqkvJxEaQe9gbQW3JH");
        PushService.setDefaultPushCallback(this, MainMenuActivity.class);
        ParseInstallation.getCurrentInstallation().saveInBackground();
    }
}
