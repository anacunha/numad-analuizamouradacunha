package edu.neu.madcourse.analuizamouradacunha.wordgame;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;

import edu.neu.madcourse.analuizamouradacunha.R;

public class Pause extends Activity implements OnClickListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Removes title bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.pause);

        View resumeGameButton = findViewById(R.id.resume_game_button);
        resumeGameButton.setOnClickListener(this);

        View instructionsButton = findViewById(R.id.instructions_button);
        instructionsButton.setOnClickListener(this);

        View quitButton = findViewById(R.id.quit_button);
        quitButton.setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.resume_game_button:
                setResult(RESULT_CANCELED, null);
                finish();
                break;
            case R.id.instructions_button:
                Intent intent = new Intent(this, Instructions.class);
                startActivity(intent);
                break;
            case R.id.quit_button:
                setResult(RESULT_OK, null);
                finish();
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                startActivity(new Intent(this, Prefs.class));
                return true;
            // More items go here (if any) ...
        }
        return false;
    }
}
