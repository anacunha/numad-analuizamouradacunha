package edu.neu.madcourse.analuizamouradacunha.trickiestpart;


import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import edu.neu.madcourse.analuizamouradacunha.R;

public class TrickiestPart extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.trickiest_part);
        Button mButton = (Button) findViewById(R.id.trickiest_part_button);
        TextView mTextViewDisplayName = (TextView) findViewById(R.id.display_name_text_view);

        try {
            final Cursor c = getContentResolver().query(ContactsContract.Profile.CONTENT_URI, null, null, null, null);
            c.moveToFirst();
            mTextViewDisplayName.setText("Display Name: " + c.getString(c.getColumnIndex(ContactsContract.Profile.DISPLAY_NAME)));
            mTextViewDisplayName.setVisibility(View.VISIBLE);
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }


        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //code to display contacts list
                TrickiestPartDialog trickiestPartDialog = new TrickiestPartDialog(TrickiestPart.this);
                trickiestPartDialog.show();
            }
        });


    }
}
