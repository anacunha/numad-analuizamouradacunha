package edu.neu.madcourse.analuizamouradacunha.foodcrunch;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import edu.neu.madcourse.analuizamouradacunha.R;

public class ContactAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Contact> data;

    public ContactAdapter(Context context, ArrayList<Contact> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data == null ? 0 : data.size();
    }

    @Override
    public Contact getItem(int position) {
        return data == null || position >= data.size() ? null : data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return data == null || position >= data.size() ? -1 : position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.food_crunch_item_contact, null);
        } else
            view = convertView;

        TextView contactName = (TextView) view.findViewById(R.id.contact_name);
        TextView contactPhoneNumber = (TextView) view.findViewById(R.id.contact_phone_number);

        Contact contact = getItem(position);
        contactName.setText(contact.getName());
        contactPhoneNumber.setText(contact.getPhoneNumber());

        return view;
    }
}
