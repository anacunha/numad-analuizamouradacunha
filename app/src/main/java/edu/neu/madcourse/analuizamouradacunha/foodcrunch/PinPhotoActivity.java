package edu.neu.madcourse.analuizamouradacunha.foodcrunch;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import java.util.ArrayList;

import edu.neu.madcourse.analuizamouradacunha.R;

public class PinPhotoActivity extends Activity {

    private static final String PHOTO_PATH = "PHOTO_PATH";
    private static final String PHOTO_TAGS = "PHOTO_TAGS";
    private LinearLayout mTags;
    private EditText mAddTag;
    private String mCurrentPhotoPath;
    private ImageView mImageView;
    private ArrayList<String> tags;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.food_crunch_pin_photo);

        mTags = (LinearLayout) findViewById(R.id.tags);
        mAddTag = (EditText) findViewById(R.id.addTag);
        mAddTag.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    in.hideSoftInputFromWindow(mAddTag.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    addTags(v.getText().toString());
                    mAddTag.setText("");
                    mAddTag.clearFocus();
                    handled = true;
                }
                return handled;
            }});

        mImageView = (ImageView) findViewById(R.id.imageView);
        mCurrentPhotoPath = getIntent().getStringExtra(PHOTO_PATH);

        tags = new ArrayList<>();
        setPhoto();
    }

    private void addTags(String tagsString) {
        for(String tag : tagsString.split(" ")) {
            if(!tags.contains(tag) && !tag.isEmpty()) {
                tags.add(tag);
                TextView mTag = new TextView(this);
                mTag.setText(tag);
                mTag.setTag(tag);
                LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                params.setMargins(15, 10, 15, 10);
                mTag.setPadding(10, 5, 10, 5);
                mTag.setBackgroundColor(Color.parseColor("#99CCFF"));
                mTags.addView(mTag, params);

                mTag.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deleteTag(v);
                    }});
            }
        }
    }

    public void deleteTag(View v) {
        // Remove from tags list
        String tag = (String) v.getTag();
        tags.remove(tag);

        // Remove from layout
        for(int i = 0; i < mTags.getChildCount(); i++) {
            View child = mTags.getChildAt(i);
            if (child.getTag() == tag)
                mTags.removeView(child);
        }
    }

    public void pinPhoto(View v) {
        addTags(mAddTag.getText().toString());
        getIntent().putExtra(PHOTO_TAGS, tags);
        setResult(Activity.RESULT_OK, getIntent());
        finish();
    }

    private void setPhoto() {
        mImageView.setImageDrawable(Drawable.createFromPath(mCurrentPhotoPath));
        mImageView.measure(mImageView.getMeasuredWidth(), mImageView.getMeasuredHeight());
    }
}
