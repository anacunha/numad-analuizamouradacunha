package edu.neu.madcourse.analuizamouradacunha.communication;

public class HighScore {

    private String userName;
    private int score;

    public HighScore (String userName, int score) {
        this.userName = userName;
        this.score = score;
    }

    public String getUserName() {
        return userName;
    }

    public int getScore() {
        return score;
    }
}
