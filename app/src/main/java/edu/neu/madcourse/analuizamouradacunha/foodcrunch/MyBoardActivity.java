package edu.neu.madcourse.analuizamouradacunha.foodcrunch;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transfermanager.Download;
import com.amazonaws.mobileconnectors.s3.transfermanager.TransferManager;
import com.amazonaws.mobileconnectors.s3.transfermanager.Upload;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.parse.CountCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import edu.neu.madcourse.analuizamouradacunha.R;

public class MyBoardActivity extends Activity implements View.OnClickListener {

    private static final String BUCKET_NAME = "foodcrunch";
    private static final String FOOD_CRUNCH_SHARED_PREF = "FOOD_CRUNCH_SHARED_PREF";
    private static final String PHOTO = "PHOTO";
    private static final String PHONE_NUMBER = "PHONE_NUMBER";
    private static final String DATE = "DATE";
    private static final String GRID_POSITION = "GRID_POSITION";
    private static final String PHOTO_PATH = "PHOTO_PATH";
    private static final String TAG = "TAG";
    private static final String NAME = "NAME";
    private static final String PHOTO_ID = "PHOTO_ID";
    private static final String PHOTO_TAGS = "PHOTO_TAGS";
    private static final String POSITION = "POSITION";
    private static final String LOG_TAG = "Food Crunch";
    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int REQUEST_PIN_PHOTO = 2;
    private static final int REQUEST_VIEW_PHOTO = 3;
    private static final int RESULT_DELETE = 4;
    private Calendar calendar;
    private File storageDir;
    private TableLayout mGridLayout;
    private LinearLayout mProgressLayout;
    private LinearLayout mButtonsLayout;
    private ImageView mImageView;
    private TextView mMonth;
    private TextView mMonday;
    private TextView mTuesday;
    private TextView mWednesday;
    private TextView mThursday;
    private TextView mFriday;
    private TextView mSaturday;
    private TextView mSunday;
    private String mCurrentPhotoPath;
    private String mUserPhoneNumber;
    private String[] grid;
    private int pinCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.food_crunch_my_board);
        setTitle("My Board");

        grid = new String[42];

        mGridLayout = (TableLayout) findViewById(R.id.gridLayout);
        mButtonsLayout = (LinearLayout) findViewById(R.id.buttonsLayout);
        mProgressLayout = (LinearLayout) findViewById(R.id.progressLayout);
        mMonth = (TextView) findViewById(R.id.month);
        mMonday = (TextView) findViewById(R.id.monday_day);
        mTuesday = (TextView) findViewById(R.id.tuesday_day);
        mWednesday = (TextView) findViewById(R.id.wednesday_day);
        mThursday = (TextView) findViewById(R.id.thursday_day);
        mFriday = (TextView) findViewById(R.id.friday_day);
        mSaturday = (TextView) findViewById(R.id.saturday_day);
        mSunday = (TextView) findViewById(R.id.sunday_day);

        storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        mUserPhoneNumber = telephonyManager.getLine1Number();

        calendar = Calendar.getInstance();
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        setCurrentWeek();
    }

    private void setCurrentWeek() {
        mMonth.setText(getCurrentMonth(calendar.get(Calendar.MONTH)));

        // Set the calendar to monday of the current week
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

        // Print dates of the current week starting on Monday
        DateFormat day = new SimpleDateFormat("dd", Locale.getDefault());
        DateFormat date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

        mMonday.setText(day.format(calendar.getTime()));
        mMonday.setTag(date.format(calendar.getTime()));
        calendar.add(Calendar.DATE, 1);
        mTuesday.setText(day.format(calendar.getTime()));
        mTuesday.setTag(date.format(calendar.getTime()));
        calendar.add(Calendar.DATE, 1);
        mWednesday.setText(day.format(calendar.getTime()));
        mWednesday.setTag(date.format(calendar.getTime()));
        calendar.add(Calendar.DATE, 1);
        mThursday.setText(day.format(calendar.getTime()));
        mThursday.setTag(date.format(calendar.getTime()));
        calendar.add(Calendar.DATE, 1);
        mFriday.setText(day.format(calendar.getTime()));
        mFriday.setTag(date.format(calendar.getTime()));
        calendar.add(Calendar.DATE, 1);
        mSaturday.setText(day.format(calendar.getTime()));
        mSaturday.setTag(date.format(calendar.getTime()));
        calendar.add(Calendar.DATE, 1);
        mSunday.setText(day.format(calendar.getTime()));
        mSunday.setTag(date.format(calendar.getTime()));

        new DownloadFilesTask().execute();
    }

    private String getCurrentMonth(int monthIndex) {
        String[] months = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
        return months[monthIndex];
    }

    public void pinPhoto(View v) {
        mImageView = (ImageView) v;
        dispatchTakePictureIntent(Integer.valueOf((String) v.getTag()));
    }

    private void dispatchTakePictureIntent(int imagePosition) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile(imagePosition);
            } catch (IOException ex) {
                // Error occurred while creating the File
                // TO DO
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            startPinPhotoActivity();
        } else if (requestCode == REQUEST_PIN_PHOTO && resultCode == RESULT_OK) {
            galleryAddPic();
            setPic();
            savePhotoInfoInParse(data.getStringArrayListExtra(PHOTO_TAGS));
        } else if (requestCode == REQUEST_VIEW_PHOTO && resultCode == RESULT_DELETE) {
            String deletedPhotoPath = data.getStringExtra(PHOTO_PATH);
            String[] file = {deletedPhotoPath};
            new DeleteFilesTask().execute(file);

            removeFromBoard(data.getIntExtra(PHOTO_ID, 0));
            int imagePosition = data.getIntExtra(POSITION, -1);
            if (imagePosition >= 0) {
                Log.d(LOG_TAG, "Cleared grid position " + imagePosition);
                grid[imagePosition] = null;
                updateWeekDay(imagePosition);
            }
        }
    }

    private void removeFromBoard(int photoID) {
        ImageView deletedPhoto = (ImageView) findViewById(photoID);
        if (deletedPhoto != null)
            deletedPhoto.setImageResource(R.drawable.grid);
    }

    private File createImageFile(int imagePosition) throws IOException {
        // Create an image file name
        String fileName = mUserPhoneNumber + "_" + getDate(imagePosition) + "_" + imagePosition + ".jpg";
        File image = new File(storageDir + "/" + fileName);

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
        uploadPhoto(f);
    }

    private void setPic() {
        // Get the dimensions of the View
        int targetW = mImageView.getWidth();
        int targetH = mImageView.getHeight();
        mImageView.setMaxWidth(targetW);
        mImageView.setMaxHeight(targetH);

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        mImageView.setImageBitmap(bitmap);
        mImageView.setOnClickListener(this);

        int imagePosition = Integer.valueOf((String) mImageView.getTag());
        checkWeekDay(imagePosition);

        Log.d(LOG_TAG, "Saved grid position " + imagePosition);
        Log.d(LOG_TAG, "Photo Path: " + mCurrentPhotoPath);
        grid[imagePosition] = mCurrentPhotoPath;
    }

    private void checkWeekDay(int imagePosition) {
        String weekDay = getWeekDay(imagePosition);
        int checkMarkId = getResources().getIdentifier(weekDay + "_check", "id", getPackageName());
        ImageView mCheckMark = (ImageView) findViewById(checkMarkId);
        mCheckMark.setImageResource(R.drawable.checked);
    }

    private void updateWeekDay(int imagePosition) {
        for (int i = (imagePosition % 7); i < 42; i += 7) {
            Log.d(LOG_TAG, "Week Position " + i);
            if (grid[i] != null)
                return;
        }

        String weekDay = getWeekDay(imagePosition);
        int checkMarkId = getResources().getIdentifier(weekDay + "_check", "id", getPackageName());
        ImageView mCheckMark = (ImageView) findViewById(checkMarkId);
        mCheckMark.setImageResource(R.drawable.unchecked);
    }

    private String getDate(int imagePosition) {
        String date = "";
        switch (imagePosition % 7) {
            case 0:
                date = (String) mMonday.getTag();
                break;
            case 1:
                date = (String) mTuesday.getTag();
                break;
            case 2:
                date = (String) mWednesday.getTag();
                break;
            case 3:
                date = (String) mThursday.getTag();
                break;
            case 4:
                date = (String) mFriday.getTag();
                break;
            case 5:
                date = (String) mSaturday.getTag();
                break;
            case 6:
                date = (String) mSunday.getTag();
                break;
        }

        return date;
    }

    private String getWeekDay(int imagePosition) {
        String weekDay = "";
        switch (imagePosition % 7) {
            case 0:
                weekDay = "monday";
                break;
            case 1:
                weekDay = "tuesday";
                break;
            case 2:
                weekDay = "wednesday";
                break;
            case 3:
                weekDay = "thursday";
                break;
            case 4:
                weekDay = "friday";
                break;
            case 5:
                weekDay = "saturday";
                break;
            case 6:
                weekDay = "sunday";
                break;
        }

        return weekDay;
    }

    private void startPinPhotoActivity() {
        Intent pinPhotoIntent = new Intent(this, PinPhotoActivity.class);
        pinPhotoIntent.putExtra(PHOTO_PATH, mCurrentPhotoPath);
        startActivityForResult(pinPhotoIntent, REQUEST_PIN_PHOTO);
    }

    @Override
    public void onClick(View v) {
        ImageView view = (ImageView) v;
        int imagePosition = Integer.valueOf((String) view.getTag());

        // If the path for that ImageView is not empty,
        // then there's a photo on that grid
        if (grid[imagePosition] != null) {
            Intent viewPinIntent = new Intent(this, ViewPhotoActivity.class);
            viewPinIntent.putExtra(POSITION, imagePosition);
            viewPinIntent.putExtra(PHOTO_PATH, grid[imagePosition]);
            viewPinIntent.putExtra(PHOTO_ID, view.getId());
            startActivityForResult(viewPinIntent, REQUEST_VIEW_PHOTO);
        } else {
            pinPhoto(v);
        }
    }

    private void uploadPhoto(File file) {
        CognitoCachingCredentialsProvider cognitoProvider = new CognitoCachingCredentialsProvider(
                MyBoardActivity.this.getApplication(),
                "us-east-1:a87ab379-f522-435c-baab-eacb5e44e04f",
                Regions.US_EAST_1
        );

        TransferManager transferManager = new TransferManager(cognitoProvider);

        Log.d(LOG_TAG, file.getName());
        Upload upload = transferManager.upload(BUCKET_NAME, file.getName(), file);

        while (!upload.isDone()) {
        }
        if (upload.isDone()) {
            Log.d(LOG_TAG, "Done Uploading");
        }
    }

    private void deletePhoto(File file) {
        CognitoCachingCredentialsProvider cognitoProvider = new CognitoCachingCredentialsProvider(
                MyBoardActivity.this.getApplication(),
                "us-east-1:a87ab379-f522-435c-baab-eacb5e44e04f",
                Regions.US_EAST_1
        );

        AmazonS3Client amazonS3Client = new AmazonS3Client(cognitoProvider);
        amazonS3Client.deleteObject(BUCKET_NAME, file.getName());
    }

    private class DownloadFilesTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            CognitoCachingCredentialsProvider cognitoProvider = new CognitoCachingCredentialsProvider(
                    MyBoardActivity.this.getApplication(),
                    "us-east-1:a87ab379-f522-435c-baab-eacb5e44e04f",
                    Regions.US_EAST_1
            );

            TransferManager transferManager = new TransferManager(cognitoProvider);

            for (int i = 0; i < 42; i++) {
                String fileName = mUserPhoneNumber + "_" + getDate(i) + "_" + i + ".jpg";
                File f = new File(storageDir + "/" + fileName);
                mCurrentPhotoPath = f.getAbsolutePath();

                try {
                    Download download = transferManager.download(BUCKET_NAME, fileName, f);
                    download.waitForCompletion();
                    grid[i] = mCurrentPhotoPath;
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            mProgressLayout.setVisibility(View.VISIBLE);
            mGridLayout.setVisibility(View.INVISIBLE);
            mButtonsLayout.setVisibility(View.INVISIBLE);
        }

        @Override
        protected void onPostExecute(Void result) {
            mProgressLayout.setVisibility(View.GONE);
            mGridLayout.setVisibility(View.VISIBLE);
            mButtonsLayout.setVisibility(View.VISIBLE);
            updateGrid();
        }
    }

    private class DeleteFilesTask extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... params) {
            for (String s : params) {
                try {
                    File deletedPhoto = new File(s);
                    Log.d(LOG_TAG, "Deleted Photo Path: " + s);
                    Log.d(LOG_TAG, "Deleted Photo Name: " + deletedPhoto.getName());
                    deletePhoto(deletedPhoto);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            mProgressLayout.setVisibility(View.VISIBLE);
            mGridLayout.setVisibility(View.INVISIBLE);
            mButtonsLayout.setVisibility(View.INVISIBLE);
        }

        @Override
        protected void onPostExecute(Void result) {
            mProgressLayout.setVisibility(View.GONE);
            mGridLayout.setVisibility(View.VISIBLE);
            mButtonsLayout.setVisibility(View.VISIBLE);
        }
    }

    private void updateGrid() {
        for (int i = 0; i < 42; i++) {
            mImageView = (ImageView) findViewById(getResources().getIdentifier("grid_" + i, "id", getPackageName()));
            mCurrentPhotoPath = grid[i];
            if (mCurrentPhotoPath != null)
                setPic();
            else
                mImageView.setImageResource(R.drawable.grid);
        }
    }

    private void clearGrid() {
        grid = new String[42];
        ((ImageView) findViewById(R.id.monday_check)).setImageResource(R.drawable.unchecked);
        ((ImageView) findViewById(R.id.tuesday_check)).setImageResource(R.drawable.unchecked);
        ((ImageView) findViewById(R.id.wednesday_check)).setImageResource(R.drawable.unchecked);
        ((ImageView) findViewById(R.id.thursday_check)).setImageResource(R.drawable.unchecked);
        ((ImageView) findViewById(R.id.friday_check)).setImageResource(R.drawable.unchecked);
        ((ImageView) findViewById(R.id.saturday_check)).setImageResource(R.drawable.unchecked);
        ((ImageView) findViewById(R.id.sunday_check)).setImageResource(R.drawable.unchecked);
    }

    public void updateWeek(View v) {
        String tag = (String) v.getTag();

        clearGrid();
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

        if (tag.equals("previous")) {
            // Set the calendar to monday of the previous week
            calendar.add(Calendar.DATE, -7);
        } else {
            // Set the calendar to monday of the next week
            calendar.add(Calendar.DATE, 7);
        }

        // Print dates of the current week starting on Monday
        DateFormat day = new SimpleDateFormat("dd", Locale.getDefault());
        DateFormat date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

        mMonth.setText(getCurrentMonth(calendar.get(Calendar.MONTH)));

        mMonday.setText(day.format(calendar.getTime()));
        mMonday.setTag(date.format(calendar.getTime()));
        calendar.add(Calendar.DATE, 1);
        mTuesday.setText(day.format(calendar.getTime()));
        mTuesday.setTag(date.format(calendar.getTime()));
        calendar.add(Calendar.DATE, 1);
        mWednesday.setText(day.format(calendar.getTime()));
        mWednesday.setTag(date.format(calendar.getTime()));
        calendar.add(Calendar.DATE, 1);
        mThursday.setText(day.format(calendar.getTime()));
        mThursday.setTag(date.format(calendar.getTime()));
        calendar.add(Calendar.DATE, 1);
        mFriday.setText(day.format(calendar.getTime()));
        mFriday.setTag(date.format(calendar.getTime()));
        calendar.add(Calendar.DATE, 1);
        mSaturday.setText(day.format(calendar.getTime()));
        mSaturday.setTag(date.format(calendar.getTime()));
        calendar.add(Calendar.DATE, 1);
        mSunday.setText(day.format(calendar.getTime()));
        mSunday.setTag(date.format(calendar.getTime()));

        new DownloadFilesTask().execute();
    }

    public void savePhotoInfoInParse(ArrayList<String> tags) {
        Log.d(LOG_TAG, "Saving Photo in Parse");

        int imagePosition = Integer.valueOf((String) mImageView.getTag());
        String date = getDate(imagePosition);

        // Create the Photo
        ParseObject photoObject = new ParseObject(PHOTO);
        photoObject.put(PHONE_NUMBER, mUserPhoneNumber);
        photoObject.put(DATE, getDate(imagePosition));
        photoObject.put(GRID_POSITION, imagePosition);
        photoObject.put(PHOTO_PATH, mUserPhoneNumber + "_" + date + "_" + imagePosition + ".jpg");

        // If there are no tags, then we save only the Photo
        if (tags.isEmpty())
            photoObject.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {
                        // Check if the pin count unlocks an achievements
                        if (!MainMenuActivity.mPref.getBoolean("achievement1", false) ||
                                !MainMenuActivity.mPref.getBoolean("achievement2", false))
                            getPinCount();
                    }
                }
            });
        else {
            for (String tag : tags) {
                // Create the Tag
                ParseObject tagObject = new ParseObject(TAG);
                tagObject.put(NAME, tag);
                tagObject.put(PHOTO, photoObject);

                // This will save both photoObject and tagObject
                tagObject.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e == null) {
                            // Check if the pin count unlocks an achievements
                            getPinCount();
                        }
                    }
                });
            }
        }
    }

    private void getPinCount() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery(PHOTO);
        query.whereEqualTo(PHONE_NUMBER, mUserPhoneNumber);
        query.countInBackground(new CountCallback() {
            @Override
            public void done(int count, ParseException e) {
                if (e == null) {
                    pinCount = count;

                    Log.d(LOG_TAG, "User has " + pinCount + " pins total");

                    MainMenuActivity.mPref = getSharedPreferences(FOOD_CRUNCH_SHARED_PREF, MODE_PRIVATE);
                    MainMenuActivity.mEditor = MainMenuActivity.mPref.edit();

                    if (!MainMenuActivity.mPref.getBoolean("achievement1", false)) {
                        if (pinCount >= 10) {
                            MainMenuActivity.mEditor.putBoolean("achievement1", true);
                            MainMenuActivity.mEditor.apply();
                            Toast toast = Toast.makeText(getApplicationContext(),
                                    "Achievement Unlocked:\n 10 Pins!", Toast.LENGTH_LONG);
                            toast.show();
                        }
                    }
                    if (!MainMenuActivity.mPref.getBoolean("achievement2", false)) {
                        if (pinCount >= 100) {
                            MainMenuActivity.mEditor.putBoolean("achievement2", true);
                            MainMenuActivity.mEditor.apply();
                            Toast toast = Toast.makeText(getApplicationContext(),
                                    "Achievement Unlocked:\n 100 Pins!", Toast.LENGTH_LONG);
                            toast.show();
                        }
                    }
                }
                else {
                    Log.d(LOG_TAG, "Error: " + e.getMessage());
                }
            }
        });
    }
}
