package edu.neu.madcourse.analuizamouradacunha.communication;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

import edu.neu.madcourse.analuizamouradacunha.R;

public class HighScores extends Activity {

    private ListView mScoresView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.high_scores);

        mScoresView = (ListView) findViewById(R.id.scores_list);
        loadScores();
    }

    private void updateScores(ArrayList<HighScore> data) {
        HighScoreAdapter adapter = new HighScoreAdapter(this, data);
        mScoresView.setAdapter(adapter);
        mScoresView.invalidate();
    }

    private void loadScores() {
        final ArrayList<HighScore> array = new ArrayList<>();
        ParseQuery<ParseObject> query = ParseQuery.getQuery(Constants.HIGH_SCORE);
        query.orderByDescending(Constants.SCORE);
        query.setLimit(10);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> scores, ParseException e) {
                if (e == null) {
                    for (ParseObject score : scores) {
                        String userName = score.getString(Constants.USER_NAME);
                        int highScore = score.getInt(Constants.SCORE);
                        array.add(new HighScore(userName, highScore));
                    }
                    updateScores(array);
                } else {
                    Toast.makeText(HighScores.this, "Failed to load high scores from Parse", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
