package edu.neu.madcourse.analuizamouradacunha.wordgame;

import java.util.ArrayList;
import java.util.Collections;

import edu.neu.madcourse.analuizamouradacunha.wordgame.Letter;

public class LetterManager {

    public static ArrayList<String> getBunch() {
        ArrayList<String> bunch = new ArrayList<>();

        for(Letter letter : Letter.values())
            for(int i = 0; i < letter.getDistribution(); i++)
                bunch.add(letter.toString());

        Collections.shuffle(bunch);
        return bunch;
    }
}