package edu.neu.madcourse.analuizamouradacunha.wordgame;

import android.app.Activity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

import edu.neu.madcourse.analuizamouradacunha.R;


public class Acknowledgements extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wordgame_acknowledgements);
        setTitle("Word Game Acknowledgements");

        TextView authorLink = (TextView) findViewById(R.id.eric_skiff);
        authorLink.setMovementMethod(LinkMovementMethod.getInstance());

        TextView authorLink2 = (TextView) findViewById(R.id.freesound);
        authorLink2.setMovementMethod(LinkMovementMethod.getInstance());

        TextView authorLink3 = (TextView) findViewById(R.id.timer_credit);
        authorLink3.setMovementMethod(LinkMovementMethod.getInstance());
    }
}
