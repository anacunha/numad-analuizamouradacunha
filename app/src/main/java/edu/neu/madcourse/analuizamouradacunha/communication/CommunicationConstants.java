package edu.neu.madcourse.analuizamouradacunha.communication;

public class CommunicationConstants
{
    public static final String TAG = "GCM_Globals";
    public static final String GCM_SENDER_ID = "411262851240";
    public static final String BASE_URL = "https://android.googleapis.com/gcm/send";
    public static final String PREFS_NAME = "GCM_Communication";
    public static final String GCM_API_KEY = "AIzaSyCZ5K0-AQ9whklJ1obMpk3LeXxc0jxjigY";
    public static final int SIMPLE_NOTIFICATION = 22;
    public static final long GCM_TIME_TO_LIVE = 60L * 60L * 24L * 7L * 4L; // 4 Weeks
    public static int mode = 0;
}