package edu.neu.madcourse.analuizamouradacunha.foodcrunch;

import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.List;

import edu.neu.madcourse.analuizamouradacunha.R;

public class ViewPhotoActivity extends FragmentActivity implements CommentDialogFragment.CommentDialogListener {

    private static final String COMMENT = "COMMENT";
    private static final String FRIEND = "FRIEND";
    private static final String LIKE = "LIKE";
    private static final String NAME = "NAME";
    private static final String PHONE_NUMBER = "PHONE_NUMBER";
    private static final String PHOTO = "PHOTO";
    private static final String PHOTO_PATH = "PHOTO_PATH";
    private static final String TAG = "TAG";
    private static final String LOG_TAG = "Food Crunch";
    private static final int RESULT_DELETE = 4;
    private ParseObject photoParseObject;
    private AlertDialog deleteDialog;
    private LinearLayout mTagsLayout;
    private LinearLayout mCommentsLayout;
    private LinearLayout mImageViewLayout;
    private LinearLayout mProgressLayout;
    private String mCurrentPhotoPath;
    private String mUserPhoneNumber;
    private ImageView mImageView;
    private ImageView mLikeButton;
    private TextView mLikeCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.food_crunch_view_photo);

        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        mUserPhoneNumber = telephonyManager.getLine1Number();

        mTagsLayout = (LinearLayout) findViewById(R.id.tags);
        mCommentsLayout = (LinearLayout) findViewById(R.id.comments);
        mImageViewLayout = (LinearLayout) findViewById(R.id.imageViewLayout);
        mProgressLayout = (LinearLayout) findViewById(R.id.progressLayout);
        mImageView = (ImageView) findViewById(R.id.imageView);
        mLikeButton = (ImageView) findViewById(R.id.likeButton);
        mLikeCount = (TextView) findViewById(R.id.likeCount);

        mProgressLayout.setVisibility(View.VISIBLE);
        mImageViewLayout.setVisibility(View.INVISIBLE);
        mTagsLayout.setVisibility(View.INVISIBLE);

        mCurrentPhotoPath = getIntent().getStringExtra(PHOTO_PATH);
        setPhoto();

        Log.d(LOG_TAG, mCurrentPhotoPath);
        if(getIntent().getBooleanExtra(FRIEND, false))
            findViewById(R.id.trashButton).setVisibility(View.GONE);

        getPhotoFromParse();
    }

    private void setPhoto() {
        mImageView.setImageDrawable(Drawable.createFromPath(mCurrentPhotoPath));
        mImageView.measure(mImageView.getMeasuredWidth(), mImageView.getMeasuredHeight());
    }

    private void addTags(ArrayList<String> tags) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(15, 10, 15, 10);

        for(String tag : tags) {
            TextView mTag = new TextView(this);
            mTag.setText(tag);
            mTag.setTag(tag);
            mTag.setPadding(10, 5, 10, 5);
            mTag.setBackgroundColor(Color.parseColor("#99CCFF"));
            mTagsLayout.addView(mTag, params);
        }
    }

    public void showDeleteDialog(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.delete_photo_prompt)
                .setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        deleteDialog.dismiss();
                        deletePhotoFromParse();
                        setResult(RESULT_DELETE, getIntent());
                        finish();
                    }
                })
                .setNegativeButton(R.string.dont_delete, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        deleteDialog.dismiss();
                    }
                })
                .setCancelable(false);

        deleteDialog = builder.create();
        deleteDialog.setCancelable(false);
        deleteDialog.setCanceledOnTouchOutside(false);
        deleteDialog.show();
    }

    public void deletePhotoFromParse() {
        if(photoParseObject != null)
            deleteCommentsFromParse();
    }

    private void deleteTagsFromParse() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery(TAG);
        query.whereEqualTo(PHOTO, photoParseObject);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(final List<ParseObject> tags, ParseException e) {
                if (e == null) {
                    if (!tags.isEmpty()) {
                        for(int i = 0; i < tags.size(); i++) {
                            final int index = i;
                            Log.d(LOG_TAG, "Deleted Photo Tag: " + tags.get(i).getString(NAME));
                            tags.get(i).deleteInBackground(new DeleteCallback() {
                                @Override
                                public void done(ParseException e) {
                                    if (e == null) {
                                        if (index == tags.size() - 1) {
                                            Log.d(LOG_TAG, "Deleted Photo: " + photoParseObject.get(PHOTO_PATH));
                                            photoParseObject.deleteInBackground();
                                        }
                                    }
                                    else {
                                        Log.d(LOG_TAG, "Error: " + e.getMessage());
                                    }
                                }
                            });
                        }
                    }
                    else {
                        Log.d(LOG_TAG, "Deleted Photo: " + photoParseObject.get(PHOTO_PATH));
                        photoParseObject.deleteInBackground();
                    }
                }
                else {
                    Log.d(LOG_TAG, "Error: " + e.getMessage());
                }
            }
        });
    }

    private void deleteCommentsFromParse() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery(COMMENT);
        query.whereEqualTo(PHOTO, photoParseObject);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(final List<ParseObject> comments, ParseException e) {
                if (e == null) {
                    for(ParseObject comment : comments) {
                        comment.deleteInBackground();
                    }
                    deleteLikesFromParse();
                }
                else {
                    Log.d(LOG_TAG, "Error: " + e.getMessage());
                }
            }
        });
    }

    private void deleteLikesFromParse() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery(LIKE);
        query.whereEqualTo(PHOTO, photoParseObject);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(final List<ParseObject> likes, ParseException e) {
                if (e == null) {
                    for(ParseObject like : likes) {
                        like.deleteInBackground();
                    }
                    deleteTagsFromParse();
                }
                else {
                    Log.d(LOG_TAG, "Error: " + e.getMessage());
                }
            }
        });
    }

    public void likePhoto(View v) {
        String tag = (String) v.getTag();

        if(tag.equals("unchecked")) {
            mLikeButton.setImageResource(R.drawable.leaf_checked);
            mLikeButton.setTag("checked");

            likePhotoOnParse();
        }
        else {
            mLikeButton.setImageResource(R.drawable.leaf_unchecked);
            mLikeButton.setTag("unchecked");

            dislikePhotoOnParse();
        }
    }

    private void getUserLike() {
        if (photoParseObject != null) {
            ParseQuery<ParseObject> query = ParseQuery.getQuery(LIKE);
            query.whereEqualTo(PHONE_NUMBER, mUserPhoneNumber);
            query.whereEqualTo(PHOTO, photoParseObject);
            query.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List<ParseObject> likes, ParseException e) {
                    if (e == null) {
                        // User has already liked this photo
                        if(!likes.isEmpty()) {
                            mLikeButton.setImageResource(R.drawable.leaf_checked);
                            mLikeButton.setTag("checked");
                        }
                        // User hasn't liked this photo yet
                        else {
                            mLikeButton.setImageResource(R.drawable.leaf_unchecked);
                            mLikeButton.setTag("unchecked");
                        }
                        getLikeCount();

                    } else {
                        Log.d(LOG_TAG, "Error: " + e.getMessage());
                    }
                }
            });
        }
    }

    private void getLikeCount() {
        if (photoParseObject != null) {
            ParseQuery<ParseObject> query = ParseQuery.getQuery(LIKE);
            query.whereEqualTo(PHOTO, photoParseObject);
            query.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List<ParseObject> likes, ParseException e) {
                    if (e == null) {
                        if(!likes.isEmpty())
                            mLikeCount.setText(String.valueOf(likes.size()));
                        else
                            mLikeCount.setText(" ");

                        mProgressLayout.setVisibility(View.GONE);
                        mImageViewLayout.setVisibility(View.VISIBLE);
                        mTagsLayout.setVisibility(View.VISIBLE);
                    } else {
                        Log.d(LOG_TAG, "Error: " + e.getMessage());
                    }
                }
            });
        }
    }

    private void likePhotoOnParse() {
        if (photoParseObject != null) {
            ParseObject likeObject = new ParseObject(LIKE);
            likeObject.put(PHOTO, photoParseObject);
            likeObject.put(PHONE_NUMBER, mUserPhoneNumber);
            likeObject.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {
                        Log.d(LOG_TAG, "Liked photo");
                        getLikeCount();
                    }
                }
            });
        }
    }

    private void dislikePhotoOnParse() {
        if (photoParseObject != null) {
            ParseQuery<ParseObject> query = ParseQuery.getQuery(LIKE);
            query.whereEqualTo(PHOTO, photoParseObject);
            query.whereEqualTo(PHONE_NUMBER, mUserPhoneNumber);
            query.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List<ParseObject> likes, ParseException e) {
                    if (e == null) {
                        if (!likes.isEmpty()) {
                            Log.d(LOG_TAG, "Disliked photo");
                            likes.get(0).deleteInBackground(new DeleteCallback() {
                                @Override
                                public void done(ParseException e) {
                                    if(e == null)
                                        getLikeCount();
                                }
                            });
                        }
                    } else {
                        Log.d(LOG_TAG, "Error: " + e.getMessage());
                    }
                }
            });
        }
    }

    public void showCommentDialog(View v) {
        // Create an instance of the dialog fragment and show it
        DialogFragment dialog = new CommentDialogFragment();
        dialog.show(getFragmentManager(), "CommentDialogFragment");
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        String name = ((EditText) dialog.getDialog().findViewById(R.id.name)).getText().toString();
        String comment = ((EditText) dialog.getDialog().findViewById(R.id.comment)).getText().toString();

        if(!name.isEmpty() && !comment.isEmpty()) {
            showComment(name, comment);
            saveCommentOnParse(name, comment);
        }
        else {
            if (name.isEmpty())
                Toast.makeText(this, "Your Name can't be empty", Toast.LENGTH_LONG).show();
            else
                Toast.makeText(this, "Your Comment can't be empty", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) { }

    private void saveCommentOnParse(String name, String comment) {
        if (photoParseObject != null) {
            ParseObject commentObject = new ParseObject(COMMENT);
            commentObject.put(PHOTO, photoParseObject);
            commentObject.put(NAME, name);
            commentObject.put(COMMENT, comment);
            commentObject.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {
                        Log.d(LOG_TAG, "Saved Comment");
                    }
                }
            });
        }
    }

    private void getCommentsFromParse() {
        if (photoParseObject != null) {
            ParseQuery<ParseObject> query = ParseQuery.getQuery(COMMENT);
            query.whereEqualTo(PHOTO, photoParseObject);
            query.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List<ParseObject> comments, ParseException e) {
                    if (e == null) {
                        for(ParseObject c : comments) {
                            String name = c.getString(NAME);
                            String comment = c.getString(COMMENT);

                            Log.d(LOG_TAG, "Comment: " + name + " - " + comment);
                            showComment(name, comment);
                        }
                        getUserLike();

                    } else {
                        Log.d(LOG_TAG, "Error: " + e.getMessage());
                    }
                }
            });
        }
    }

    private void showComment(String name, String comment) {
        TextView mComment = new TextView(this);
        mComment.setPadding(0, 0, 0, 5);
        mComment.setText(Html.fromHtml("<b>" + name + "</b> - " + comment));
        mCommentsLayout.addView(mComment);
    }

    private void getPhotoFromParse() {
        String[] photoPath = mCurrentPhotoPath.split("/");
        String photoFileName = photoPath[photoPath.length - 1];
        Log.d(LOG_TAG, "Photo File Name: " + photoFileName);

        ParseQuery<ParseObject> query = ParseQuery.getQuery(PHOTO);
        query.whereEqualTo(PHOTO_PATH, photoFileName);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> photos, ParseException e) {
                if (e == null) {
                    if (!photos.isEmpty()) {
                        photoParseObject = photos.get(0);
                        Log.d(LOG_TAG, "Photo Parse ID: " + photoParseObject.getObjectId());
                        getTagsFromParse();
                    }
                }
                else {
                    Log.d(LOG_TAG, "Error: " + e.getMessage());
                }
            }
        });
    }

    private void getTagsFromParse() {
        final ArrayList<String> tagsList = new ArrayList<>();

        ParseQuery<ParseObject> query = ParseQuery.getQuery(TAG);
        query.whereEqualTo(PHOTO, photoParseObject);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> tags, ParseException e) {
                if (e == null) {
                    for(ParseObject tag : tags) {
                        tagsList.add(tag.getString(NAME));
                        Log.d(LOG_TAG, "Photo Tag: " + tag.getString(NAME));
                    }
                    addTags(tagsList);
                    getCommentsFromParse();
                }
                else {
                    Log.d(LOG_TAG, "Error: " + e.getMessage());
                }
            }
        });
    }
}
