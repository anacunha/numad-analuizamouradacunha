package edu.neu.madcourse.analuizamouradacunha.foodcrunch;

public class LeaderboardEntry implements Comparable<LeaderboardEntry> {

    private String userPhoneNumber;
    private int userPins;

    public LeaderboardEntry(String userPhoneNumber, int userPins) {
        this.userPhoneNumber = userPhoneNumber;
        this.userPins = userPins;
    }

    public String getUserPhoneNumber() {
        return userPhoneNumber;
    }

    public int getUserPins() {
        return userPins;
    }


    @Override
    public int compareTo(LeaderboardEntry another) {

        // We want leaderboard in DESCENDING order
        return another.userPins - this.userPins;
    }

    @Override
    public String toString() {
        return userPhoneNumber + " - " + userPins + " pins";
    }
}
