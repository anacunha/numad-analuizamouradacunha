package edu.neu.madcourse.analuizamouradacunha;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.TextView;

public class About extends Activity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.anamoura);

        View acknowledgementsButton = findViewById(R.id.acknowledgements_button);
        acknowledgementsButton.setOnClickListener(this);

        TextView IMEI = (TextView) findViewById(R.id.imei);
        TelephonyManager telephonyManager = (TelephonyManager)getSystemService(this.TELEPHONY_SERVICE);
        if(telephonyManager.getDeviceId() != null)
            IMEI.setText(telephonyManager.getDeviceId());
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.acknowledgements_button) {
            Intent intent = new Intent(this, Acknowledgements.class);
            startActivity(intent);
        }
    }
}

