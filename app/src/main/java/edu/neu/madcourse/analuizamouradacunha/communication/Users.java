package edu.neu.madcourse.analuizamouradacunha.communication;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

import edu.neu.madcourse.analuizamouradacunha.R;

public class Users extends Activity implements AdapterView.OnItemClickListener {

    private static final String TAG = "Communication";
    private static final String RECIPIENT_USER_NAME = "recipientUserName";
    private static final String RECIPIENT_REGISTRATION_ID = "recipientRegistrationID";
    private static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private Context context;
    private ListView usersView;
    private String userName;
    private String registrationID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.users);

        context = getApplicationContext();
        usersView = (ListView) findViewById(R.id.users_list);
        usersView.setOnItemClickListener(this);
        getRegistration();
        loadUsers();
        getUserInfo();
    }

    private void updateUsers(ArrayList<User> data) {
        UserAdapter adapter = new UserAdapter(this, android.R.layout.simple_list_item_1, data);
        usersView.setAdapter(adapter);
        usersView.invalidate();
    }

    private void loadUsers() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery(Constants.USER);
        final ArrayList<User> array = new ArrayList<>();
        query.orderByAscending(Constants.USER_NAME);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> users, ParseException e) {
                if (e == null) {
                    for (ParseObject user : users) {
                        String registrationID = user.getString(Constants.REGISTRATION_ID);
                        String userName = user.getString(Constants.USER_NAME);
                        array.add(new User(user, userName));
                    }
                    updateUsers(array);
                } else {
                    Toast.makeText(Users.this, "Failed to load users from Parse", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void getUserInfo() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery(Constants.USER);
        query.whereEqualTo(Constants.REGISTRATION_ID, registrationID);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
                if (e == null) {
                    for (ParseObject user : parseObjects) {
                        userName = user.getString(Constants.USER_NAME);
                        Log.d(TAG, "Identified User: " + userName);
                    }
                }
                else
                    Log.d(TAG, "Error: " + e.getMessage());
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {

        if(TextUtils.isEmpty(registrationID))
            Toast.makeText(this, "Your must register before sending messages", Toast.LENGTH_LONG).show();
        else {
            User user = (User) usersView.getItemAtPosition(position);
            String userRegistrationID = user.getParseObject().getString(Constants.REGISTRATION_ID);
            Log.d(TAG, "Selected user: " + user.getUserName() + " (" + userRegistrationID + ")");

            Intent intent = new Intent(this, UserInfo.class);
            intent.putExtra(Constants.USER_NAME, userName);
            intent.putExtra(Constants.REGISTRATION_ID, registrationID);
            intent.putExtra(RECIPIENT_USER_NAME, user.getUserName());
            intent.putExtra(RECIPIENT_REGISTRATION_ID, userRegistrationID);
            startActivity(intent);
        }
    }

    private void getRegistration() {
        if (checkPlayServices())
            registrationID = getRegistrationId(context);
    }

    @SuppressLint("NewApi")
    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION,Integer.MIN_VALUE);
        Log.i(TAG, String.valueOf(registeredVersion));
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }

    private SharedPreferences getGCMPreferences(Context context) {
        return getSharedPreferences(Communication.class.getSimpleName(), Context.MODE_PRIVATE);
    }

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }
}
