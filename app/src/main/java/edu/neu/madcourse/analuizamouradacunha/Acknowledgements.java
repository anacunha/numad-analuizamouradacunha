package edu.neu.madcourse.analuizamouradacunha;

import android.app.Activity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;


public class Acknowledgements extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acknowledgements);

        TextView authorLink = (TextView) findViewById(R.id.eric_skiff);
        authorLink.setMovementMethod(LinkMovementMethod.getInstance());

        TextView authorLink2 = (TextView) findViewById(R.id.eric_skiff2);
        authorLink2.setMovementMethod(LinkMovementMethod.getInstance());

        TextView authorLink3 = (TextView) findViewById(R.id.lloyd_humphreys);
        authorLink3.setMovementMethod(LinkMovementMethod.getInstance());

        TextView iconLink = (TextView) findViewById(R.id.spade_playing_card);
        iconLink.setMovementMethod(LinkMovementMethod.getInstance());
    }
}
