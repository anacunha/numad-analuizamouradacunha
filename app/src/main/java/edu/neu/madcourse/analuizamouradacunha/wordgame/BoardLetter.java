package edu.neu.madcourse.analuizamouradacunha.wordgame;

public class BoardLetter {

    private Letter letter;

    public BoardLetter(Letter letter) {
        this.letter = letter;
    }

    public Letter getLetter() {
        return letter;
    }

    public String toString() {
        return letter.toString();
    }

    public char getChar() {
        return letter.toString().charAt(0);
    }
}
