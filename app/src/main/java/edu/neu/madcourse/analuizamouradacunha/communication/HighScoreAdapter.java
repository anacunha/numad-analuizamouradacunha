package edu.neu.madcourse.analuizamouradacunha.communication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import edu.neu.madcourse.analuizamouradacunha.R;

public class HighScoreAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<HighScore> data;

    public HighScoreAdapter(Context context, ArrayList<HighScore> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data == null ? 0 : data.size();
    }

    @Override
    public HighScore getItem(int position) {
        return data == null || position >= data.size() ? null : data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return data == null || position >= data.size() ? -1 : position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.item_cognito_high_score, null);
        } else
            view = convertView;

        TextView tvUserName = (TextView) view.findViewById(R.id.tv_item_user_name);
        TextView tvScore = (TextView) view.findViewById(R.id.tv_item_score);

        HighScore highScore = getItem(position);
        tvUserName.setText(highScore.getUserName());
        tvScore.setText(highScore.getScore() + "");

        return view;
    }
}
