package edu.neu.madcourse.analuizamouradacunha.dictionary;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.ArrayList;

import edu.neu.madcourse.analuizamouradacunha.R;

public class Dictionary extends Activity implements View.OnClickListener {

    private Context context;
    private ArrayList<String> words;
    private ArrayAdapter<String> arrayAdapter;
    private EditText dictionaryInput;
    private DatabaseHelper databaseHelper;
    private SQLiteDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dictionary);
        this.setTitle(R.string.dictionary_title);

        context = this;
        words = new ArrayList<>();
        arrayAdapter = new ArrayAdapter<>(this, R.layout.custom_simple_list_item_1, words);

        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(arrayAdapter);

        /*
         * Listener for text changes on the Dictionary input.
         */
        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String string = s.toString().trim().toLowerCase();
                if(string.length() > 2) {
                    final Cursor cursor = database.rawQuery("SELECT word FROM words WHERE word = '" + string + "'", null);
                    String word;
                    if (cursor != null) {
                        try {
                            if (cursor.moveToFirst()) {
                                word = cursor.getString(0);
                                if(!words.contains(word)) {
                                    Music.play(context, R.raw.beep);
                                    words.add(word);
                                    arrayAdapter.notifyDataSetChanged();
                                }
                            }
                        } finally {
                            cursor.close();
                        }
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {}
        };

        dictionaryInput = (EditText) findViewById(R.id.dictionaryInput);
        dictionaryInput.addTextChangedListener(textWatcher);

        View returnToMenuButton = findViewById(R.id.return_to_menu_button);
        returnToMenuButton.setOnClickListener(this);

        View acknowledgementsButton = findViewById(R.id.acknowledgements_button);
        acknowledgementsButton.setOnClickListener(this);

        View clearButton = findViewById(R.id.clear_button);
        clearButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.return_to_menu_button:
                finish();
                break;
            case R.id.acknowledgements_button:
                intent = new Intent(this, Acknowledgements.class);
                startActivity(intent);
                break;
            case R.id.clear_button:
                dictionaryInput.setText("");
                arrayAdapter.clear();
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        new AsyncTask<Void, Void, Void>() {

            LinearLayout progressLayout = (LinearLayout) findViewById(R.id.progressLayout);

            @Override
            protected Void doInBackground(Void... params) {
                databaseHelper = new DatabaseHelper(context);
                databaseHelper.initializeDataBase();
                Log.d("DB", "Initialized database helper");

                try {
                    // A reference to the database can be obtained after initialization.
                    database = databaseHelper.getWritableDatabase();
                    Log.d("DB", "Referenced database");
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPreExecute() {
                progressLayout.setVisibility(View.VISIBLE);
            }

            @Override
            protected void onPostExecute(Void result) {
                progressLayout.setVisibility(View.GONE);
            }
        }.execute();
    }

    @Override
    public void onPause() {
        super.onPause();
        Music.stop(this);

        try {
            databaseHelper.close();
            Log.d("DB", "Closing Database Helper");
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            database.close();
            Log.d("DB", "Closing Database");
        }
    }
}
