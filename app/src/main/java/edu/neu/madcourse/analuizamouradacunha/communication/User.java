package edu.neu.madcourse.analuizamouradacunha.communication;

import com.parse.ParseObject;

public class User {

    private ParseObject parseObject;
    private String userName;

    public User (ParseObject parseObject, String userName) {
        this.parseObject = parseObject;
        this.userName = userName;
    }

    public ParseObject getParseObject() {
        return parseObject;
    }

    public String getUserName() {
        return userName;
    }
}
