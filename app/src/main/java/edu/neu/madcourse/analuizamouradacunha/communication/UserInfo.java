package edu.neu.madcourse.analuizamouradacunha.communication;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.neu.madcourse.analuizamouradacunha.R;

public class UserInfo extends Activity implements View.OnClickListener {

    private static final String TAG = "Communication";
    private static final String USER_NAME = "userName";
    private static final String REGISTRATION_ID = "registrationID";
    private static final String RECIPIENT_USER_NAME = "recipientUserName";
    private static final String RECIPIENT_REGISTRATION_ID = "recipientRegistrationID";

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private String recipientUserName;
    private String recipientRegistrationID;
    private String userName;
    private String registrationID;
    private Context context;
    private Button mSend;
    private TextView mUserName;
    private GoogleCloudMessaging gcm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.user);

        userName = getIntent().getExtras().getString(USER_NAME);
        registrationID = getIntent().getExtras().getString(REGISTRATION_ID);
        recipientUserName = getIntent().getExtras().getString(RECIPIENT_USER_NAME);
        recipientRegistrationID = getIntent().getExtras().getString(RECIPIENT_REGISTRATION_ID);

        mUserName = (TextView) findViewById(R.id.user_name);
        mUserName.setText(recipientUserName);

        mSend = (Button) findViewById(R.id.send_button);
        mSend.setOnClickListener(this);

        context = getApplicationContext();

        if(checkPlayServices())
            gcm = GoogleCloudMessaging.getInstance(this);
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View v) {

        switch(v.getId()) {

            case R.id.send_button:
                String message = ((EditText) findViewById(R.id.message)).getText().toString();
                sendMessage(message);
                break;
        }
    }

    @SuppressLint("NewApi")
    private void sendMessage(final String message) {
        if (registrationID == null || registrationID.equals("")) {
            Toast.makeText(this, "You must register first", Toast.LENGTH_LONG).show();
            return;
        }
        if (message.isEmpty()) {
            Toast.makeText(this, "Empty Message", Toast.LENGTH_LONG).show();
            return;
        }

        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                List<String> regIds = new ArrayList<>();
                String reg_device = recipientRegistrationID;
                int nIcon = R.drawable.ic_stat_cloud;
                int nType = CommunicationConstants.SIMPLE_NOTIFICATION;
                Map<String, String> msgParams;
                msgParams = new HashMap<>();
                msgParams.put("data.alertText", "New Message");
                msgParams.put("data.titleText", userName);
                msgParams.put("data.contentText", message);
                msgParams.put("data.nIcon", String.valueOf(nIcon));
                msgParams.put("data.nType", String.valueOf(nType));
                GCMNotification gcmNotification = new GCMNotification();
                regIds.clear();
                regIds.add(reg_device);
                gcmNotification.sendNotification(msgParams, regIds, UserInfo.this);

                return "Sending message...";
            }

            @Override
            protected void onPostExecute(String msg) {
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
            }
        }.execute(null, null, null);
    }
}
