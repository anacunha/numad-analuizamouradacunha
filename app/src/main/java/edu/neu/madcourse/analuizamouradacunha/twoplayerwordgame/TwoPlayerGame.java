package edu.neu.madcourse.analuizamouradacunha.twoplayerwordgame;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.gson.Gson;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.neu.madcourse.analuizamouradacunha.R;
import edu.neu.madcourse.analuizamouradacunha.communication.Communication;
import edu.neu.madcourse.analuizamouradacunha.communication.CommunicationConstants;
import edu.neu.madcourse.analuizamouradacunha.communication.Constants;
import edu.neu.madcourse.analuizamouradacunha.communication.GCMNotification;
import edu.neu.madcourse.analuizamouradacunha.dictionary.DatabaseHelper;
import edu.neu.madcourse.analuizamouradacunha.wordgame.BoardLetter;
import edu.neu.madcourse.analuizamouradacunha.wordgame.Letter;
import edu.neu.madcourse.analuizamouradacunha.wordgame.LetterManager;
import edu.neu.madcourse.analuizamouradacunha.wordgame.MusicBackground;
import edu.neu.madcourse.analuizamouradacunha.wordgame.MusicSound;
import edu.neu.madcourse.analuizamouradacunha.wordgame.Pause;
import edu.neu.madcourse.analuizamouradacunha.wordgame.Prefs;
import edu.neu.madcourse.analuizamouradacunha.wordgame.Tile;

public class TwoPlayerGame extends Activity implements OnClickListener {

    private static final String CONTINUE = "CONTINUE";
    private static final String NEW_GAME = "NEW_GAME";
    private static final String BOARD = "BOARD";
    private static final String IS_WORD = "IS_WORD";
    private static final String LETTERS = "LETTERS";
    private static final String POINTS = "POINTS";
    private static final String ELAPSED_TIME = "ELAPSED_TIME";
    private static final String BUNCH = "BUNCH";
    private static final String PREFS = "PREFS";
    private static final String TAG = "WordFade";
    private static final int EXIT = 3948;

    private String registrationID;
    private String opponentRegistrationID;
    private String playerName;
    private ParseObject player;
    private ParseObject game;
    private ParseObject match;

    private int points;
    private long startTime;
    private long elapsedTime;
    private char[] board;
    private boolean[] isWord;
    private boolean over;

    private Context context;
    private Button dumpButton;
    private ImageView letterSelectedView;
    private ArrayList<String> letters;
    private ArrayList<String> bunch;
    private Vibrator vibrator;
    private DatabaseHelper databaseHelper;
    private SQLiteDatabase database;
    private TextView scoreView;
    private TextView timerView;
    private SharedPreferences sharedPreferences;
    private AlertDialog gameOverDialog;
    private Gson gson;

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";

    private Handler timerHandler = new Handler();
    private Runnable timerRunnable = new Runnable() {

        @Override
        public void run() {
            elapsedTime = System.currentTimeMillis() - startTime;
            int seconds = (int) (elapsedTime / 1000);
            seconds = seconds % 60;

            timerView.setText(String.format("%02ds", 30 - seconds));

            if (seconds >= 25)
                timerWarning();

            if (seconds == 30 || points < 0) {
                timerHandler.removeCallbacks(timerRunnable);

                if (playerName != null && !playerName.isEmpty())
                    saveScore(points);

                showGameOverDialog();
                over = true;
                updateGameOnline(false);
            } else
                timerHandler.postDelayed(this, 500);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        Log.d(TAG, "onCreate");

        // Removes title bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.wordfade);
        scoreView = (TextView) findViewById(R.id.score);
        timerView = (TextView) findViewById(R.id.timer);
        dumpButton = (Button) findViewById(R.id.dump_button);
        vibrator = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
        sharedPreferences = getSharedPreferences(PREFS, MODE_PRIVATE);

        letters = new ArrayList<>(21);
        bunch = new ArrayList<>();
        gson = new Gson();

        // If user is starting a New Game
        if (getIntent().getBooleanExtra(NEW_GAME, false)) {
            Log.d(TAG, "New Game");
            clearGame();
            bunch = LetterManager.getBunch();
            getInitialLetters();
        }

        // Gets player info
        if (checkPlayServices())
            registrationID = getRegistrationId(context);
        if (!TextUtils.isEmpty(registrationID))
            getMatchInfo();

        // Points
        points = sharedPreferences.getInt(POINTS, 0);
        Log.d(TAG, "Points: " + points);

        // Elapsed Time
        elapsedTime = sharedPreferences.getLong(ELAPSED_TIME, 0);
        Log.d(TAG, "Elapsed Time: " + elapsedTime);

        // Board (char[])
        if (sharedPreferences.getString(BOARD, null) == null)
            board = new char[225];
        else
            board = sharedPreferences.getString(BOARD, null).toCharArray();

        // IsWord (boolean[])
        if (sharedPreferences.getBoolean(IS_WORD, false))
            isWord = loadIsWord();
        else
            isWord = new boolean[225];

        // Letters (ArrayList<String>)
        if (sharedPreferences.getString(LETTERS, null) != null)
            letters = loadLetters();

        // Bunch
        if (sharedPreferences.getString(BUNCH, null) != null)
            bunch = loadBunch();

        addTileListeners();
        updatePoints();
    }

    private void getInitialLetters() {
        for (int i = 0; i < 21; i++)
            if (!bunch.isEmpty())
                letters.add(bunch.remove(0));

        Log.d(TAG, "Player's initial letters: " + letters);
    }

    private void displayRackLetters() {
        for (int i = 0; i < 21; i++) {

            int viewID = getResources().getIdentifier("rack" + i, "id", getPackageName());
            ImageView letterView = (ImageView) findViewById(viewID);

            if (letterView != null) {
                if (i < letters.size()) {
                    BoardLetter letter = new BoardLetter(Letter.valueOf(letters.get(i)));
                    int drawableID = getResources().getIdentifier(letter.toString().toLowerCase() + "_selector", "drawable", getPackageName());
                    letterView.setImageResource(drawableID);
                    letterView.setTag(letter);
                    letterView.setOnClickListener(this);
                } else {
                    letterView.setImageDrawable(null);
                }

            }
        }
    }

    private void addTileListeners() {
        for (int i = 0; i < 15; i++) {
            for (int j = 0; j < 15; j++) {
                int viewID = getResources().getIdentifier("cell" + i + "_" + j, "id", getPackageName());
                ImageView tileView = (ImageView) findViewById(viewID);

                if (tileView != null) {
                    Tile tile = new Tile(i, j);

                    // Add existing letter to that tile
                    if (board[(i * 15) + j] != 0)
                        tile.setLetter(new BoardLetter(Letter.valueOf(String.valueOf(board[(i * 15) + j]))));

                    tileView.setTag(tile);
                    tileView.setOnClickListener(this);
                }
            }
        }
    }

    private boolean split() {

        Log.d(TAG, "Points: " + points);

        int size = letters.size();

        if (size < 21 && !bunch.isEmpty()) {
            String letter = bunch.remove(0);
            letters.add(letter);

            int splitPoints = getSplitPoints(size);
            points = points + splitPoints;

            if (splitPoints < 0)
                Toast.makeText(context, "Split! Lost " + (-1 * splitPoints) + " points.", Toast.LENGTH_SHORT).show();
            else if (splitPoints == 0)
                Toast.makeText(context, "Split!", Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(context, "Split! Got " + splitPoints + " points.", Toast.LENGTH_SHORT).show();

            Log.d(TAG, "Split");
            Log.d(TAG, "Received letter " + letter);
            Log.d(TAG, "Split points: " + splitPoints);
            Log.d(TAG, "Points: " + points);
            return true;
        }
        return false;
    }

    public void split(View v) {
        if (letters.size() >= 21)
            Toast.makeText(context, "Can't split with 21 letters on the rack", Toast.LENGTH_SHORT).show();
        else if (bunch.isEmpty())
            Toast.makeText(context, "There are no letters left on the bunch", Toast.LENGTH_LONG).show();
        else {
            split();
            updatePoints();
            displayRackLetters();
        }
    }

    private int getSplitPoints(int size) {
        if (size == 0)
            return 15;
        else if (size == 1)
            return 10;
        else if (size == 2)
            return 8;
        else if (size == 3)
            return 6;
        else if (size == 4)
            return 4;
        else if (size <= 10)
            return 0;
        else
            return -5;
    }

    private boolean dump(BoardLetter dumpedLetter) {

        // Can only dump if there are more than 3 letters on the pile
        if (letters.size() <= 18 && bunch.size() >= 3) {

            Log.d(TAG, "Points: " + points);

            // Player returns his/her letter to the pile before getting new ones
            bunch.add(dumpedLetter.getLetter().toString());
            Collections.shuffle(bunch);
            letters.remove(dumpedLetter.toString());

            int dumpPoints = 5;

            Log.d(TAG, "Dumped " + dumpedLetter.toString() + " (-5 points)");

            for (int i = 0; i < 3; i++) {
                String letter = bunch.remove(0);
                letters.add(letter);
                dumpPoints = dumpPoints + (Letter.valueOf(letter).getPoints() * 2);

                Log.d(TAG, "Received " + letter + " (-" + Letter.valueOf(letter).getPoints() * 2 + " points)");
            }

            points = points - dumpPoints;
            Toast.makeText(context, "Dump! Lost " + dumpPoints + " points.", Toast.LENGTH_SHORT).show();
            Log.d(TAG, "Points: " + points);
            return true;
        } else if (letters.size() > 18)
            Toast.makeText(context, "Can't dump with more than 18 letters on the rack", Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(context, "There are not enough letters left on the bunch", Toast.LENGTH_LONG).show();

        return false;
    }

    public void dump(View v) {
        if (letterSelectedView != null && letterSelectedView.getTag() instanceof BoardLetter) {
            dump((BoardLetter) letterSelectedView.getTag());
            displayRackLetters();
            updatePoints();
            deselectLetter();
        }
    }

    @Override
    public void onClick(View v) {

        if (v instanceof ImageView) {

            // If player selected a Letter of the rack
            if (v.getTag() instanceof BoardLetter) {

                // Shows Dump button
                showDumpButton();

                vibrator.vibrate(50);
                // Selects new letter when no other letter is selected
                if (letterSelectedView == null) {

                    BoardLetter letterSelected = (BoardLetter) v.getTag();
                    letterSelectedView = (ImageView) v;
                    letterSelectedView.setAlpha(100);

                    Log.d(TAG, "Letter " + letterSelected.toString() + " selected (position: " + v.getId() + ")");
                }
                // Selects new letter when there was a previously selected letter
                else {

                    BoardLetter letterSelected = (BoardLetter) letterSelectedView.getTag();
                    Log.d(TAG, "Letter " + letterSelected.toString() + " deselected (position: " + v.getId() + ")");

                    // Deselects previously selected letter
                    letterSelectedView.setAlpha(255);

                    // Selects same letter that was previously selected
                    if (letterSelected == v.getTag()) {

                        deselectLetter();

                    } else {
                        // Selects new letter
                        letterSelected = (BoardLetter) v.getTag();
                        letterSelectedView = (ImageView) v;
                        letterSelectedView.setAlpha(100);

                        Log.d(TAG, "Letter " + letterSelected.toString() + " selected (position: " + v.getId() + ")");
                    }
                }
            }
            // Player selected a tile on the grid
            else if (v.getTag() instanceof Tile) {

                Tile tile = (Tile) v.getTag();
                int r = tile.getRow();
                int c = tile.getColumn();

                // If a letter from the rack is currently selected
                // and tile is not part of a word
                if (letterSelectedView != null && !isWord[(r * 15) + c]) {

                    // If tile had a letter before, return it to the rack
                    if (tile.getLetter() != null)
                        returnLetterToRack(tile.getLetter());

                    // Places selected letter on tile
                    vibrator.vibrate(50);
                    BoardLetter letterSelected = (BoardLetter) letterSelectedView.getTag();
                    tile.setLetter(letterSelected);
                    letterSelectedView.setAlpha(255);
                    ((ImageView) v).setImageDrawable(letterSelectedView.getDrawable());

                    board[(r * 15) + c] = letterSelected.getChar();
                    if (detectWords(tile.getRow(), tile.getColumn()))
                        validWordUpdate();

                    Log.d(TAG, "Added " + tile.getLetter().toString() + " to tile (" + tile.getRow() + "," + tile.getColumn() + ")");

                    // Removes selected letter from rack view
                    letters.remove(letterSelected.getLetter().toString());
                    displayRackLetters();

                    deselectLetter();

                    Log.d(TAG, "Letter " + letterSelected.toString() + " deselected (position: " + v.getId() + ")");
                    Log.d(TAG, "Removed " + letterSelected.toString() + " from rack");
                }
                // If a letter from the rack is currently selected
                // and tile is part of a word
                else if (letterSelectedView != null) {

                    // Only adds letter to tile if it's the same as current one
                    // Resets times
                    BoardLetter letterSelected = (BoardLetter) letterSelectedView.getTag();
                    if (letterSelected.getLetter() == tile.getLetter().getLetter()) {
                        vibrator.vibrate(50);
                        // Removes selected letter from rack view
                        letterSelectedView.setImageDrawable(null);

                        // Deselects letter
                        letterSelectedView.setAlpha(255);
                        letters.remove(letterSelected.getLetter().toString());
                        displayRackLetters();

                        deselectLetter();

                        // Resets timer
                        startTime = System.currentTimeMillis();
                        cancelTimerWarning();
                        startLettersFade(getRemainingTime());
                    }
                }
                // No letter from the rack is selected
                // Clears tile if it's not part of a word yet
                else if (tile.getLetter() != null && !isWord[(r * 15) + c]) {

                    board[(r * 15) + c] = 0;

                    // Returns letter to rack
                    returnLetterToRack(tile.getLetter());

                    // Clears tile
                    Log.d(TAG, "Cleared " + tile.getLetter().toString() + " from tile (" + tile.getRow() + "," + tile.getColumn() + ")");
                    tile.setLetter(null);
                    ((ImageView) v).setImageResource(R.drawable.tile);
                }
            }
        }
    }

    private void returnLetterToRack(BoardLetter letter) {
        letters.add(letter.getLetter().toString());
        displayRackLetters();
    }

    @Override
    public void onResume() {
        super.onResume();

        new AsyncTask<Void, Void, Void>() {

            LinearLayout progressLayout = (LinearLayout) findViewById(R.id.progressLayout);

            @Override
            protected Void doInBackground(Void... params) {
                databaseHelper = new DatabaseHelper(context);
                databaseHelper.initializeDataBase();
                Log.d("DB", "Initialized database helper");


                try {
                    // A reference to the database can be obtained after initialization.
                    database = databaseHelper.getWritableDatabase();
                    Log.d("DB", "Referenced database");
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPreExecute() {
                progressLayout.setVisibility(View.VISIBLE);
            }

            @Override
            protected void onPostExecute(Void result) {
                progressLayout.setVisibility(View.GONE);
            }
        }.execute();

        MusicBackground.play(this, R.raw.hhavok_intro);
        drawBoard();
        displayRackLetters();
        resumeFading();
        letters.trimToSize();
        updateGameOnline(false);
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
        MusicBackground.stop(this);
        MusicSound.stop(this);

        try {
            databaseHelper.close();
            Log.d("DB", "Closing Database Helper");
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (database != null)
                database.close();
            Log.d("DB", "Closing Database");
        }

        stopFading();
        updateGameOnline(true);

        if (gameOverDialog != null)
            gameOverDialog.dismiss();
        else
            saveGame();
    }

    // Detects new words that could be formed by the placement of a tile at the given position
    private boolean detectWords(int row, int column) {
        String horizontal = getHorizontalWord(row, column);
        String vertical = getVerticalWord(row, column);

        if (horizontal == null && vertical == null)
            return false;

        // First word on board doesn't have to be connected to anything
        // Other words must be connected
        if (!hasWord() || isConnected(row, column))
            return detectWords(row, column, horizontal, vertical);

        return false;
    }

    private boolean detectWords(int row, int column, String horizontal, String vertical) {
        if (isValidWord(horizontal) && isValidWord(vertical)) {

            // Check if all the letters of the new word are part of
            // vertically and horizontally valid words.
            if (checkAdjacentWordsValid(row, column)) {

                if (horizontal != null) {
                    int pointsHorizontal = getPointsHorizontalWord(row, column);
                    points = points + pointsHorizontal;

                    sendMessage(playerName + " has placed \'" + horizontal + "\' for " + pointsHorizontal + " points!");
                }

                if (vertical != null) {
                    int pointsVertical = getPointsVerticalWord(row, column);
                    points = points + pointsVertical;

                    sendMessage(playerName + " has placed \'" + vertical + "\' for " + pointsVertical + " points!");
                }

                return true;
            } else {
                Log.d(TAG, "Adjacent words are invalid");
                return false;
            }
        }
        return false;
    }

    private boolean checkAdjacentWordsValid(int row, int column) {

        Log.d(TAG, "Row: " + row + " Column: " + column);

        // We already know the horizontal word is valid, now
        // we have to check if every letter in the horizontal
        // word is part of a valid vertical word.
        for (Integer i : getVerticalRange(row, column)) {
            Log.d(TAG, "Letters of Vertical Word");
            Log.d(TAG, "Board position: " + ((i * 15) + column));
            Log.d(TAG, "Letter: " + board[(i * 15) + column] + " (position: " + i + "," + column + ")");
            if (!isValidWord(getHorizontalWord(i, column)))
                return false;
        }

        // We already know the vertical word is valid, now
        // we have to check if every letter in the vertical
        // word is part of a valid horizontal word.
        for (Integer i : getHorizontalRange(row, column)) {
            Log.d(TAG, "Letters of Horizontal Word");
            Log.d(TAG, "Board position: " + ((row * 15) + i));
            Log.d(TAG, "Letter: " + board[(row * 15) + i] + " (position: " + row + "," + i + ")");
            if (!isValidWord(getVerticalWord(row, i)))
                return false;
        }

        return true;
    }

    private void startLettersFade(long time) {
        for (int i = 0; i < 15; i++)
            for (int j = 0; j < 15; j++)
                if (isWord[(i * 15) + j])
                    startLetterFade(i, j, time);
    }

    private void startLetterFade(int row, int column, long time) {
        int viewID = getResources().getIdentifier("cell" + row + "_" + column, "id", getPackageName());
        ImageView tileView = (ImageView) findViewById(viewID);

        if (tileView != null) {
            tileView.clearAnimation();
            final Animation animation = new AlphaAnimation((float) time / 30000, 0); // Change alpha from fully visible to invisible
            animation.setDuration(time);
            animation.setInterpolator(new LinearInterpolator());
            animation.setRepeatCount(0);
            tileView.startAnimation(animation);
        }
    }

    private void stopLettersFade() {
        for (int i = 0; i < 15; i++)
            for (int j = 0; j < 15; j++)
                if (isWord[(i * 15) + j])
                    stopLetterFade(i, j);
    }

    private void stopLetterFade(int row, int column) {
        int viewID = getResources().getIdentifier("cell" + row + "_" + column, "id", getPackageName());
        ImageView tileView = (ImageView) findViewById(viewID);

        if (tileView != null)
            tileView.clearAnimation();
    }

    private ArrayList<Integer> getHorizontalRange(int row, int column) {

        ArrayList<Integer> range = new ArrayList<>();

        // Goes backward until end of board or whitespace
        for (int i = column; i >= 0; i--) {
            if (board[(row * 15) + i] == 0)
                break;

            range.add(0, i);
        }

        // Goes forward until end of board or whitespace
        for (int i = column + 1; i < 15; i++) {
            if (board[(row * 15) + i] == 0)
                break;

            range.add(i);
        }

        return range;
    }

    private ArrayList<Integer> getVerticalRange(int row, int column) {

        ArrayList<Integer> range = new ArrayList<>();

        // Goes backward until end of board or whitespace
        for (int i = row; i >= 0; i--) {
            if (board[(i * 15) + column] == 0)
                break;

            range.add(0, i);
        }

        // Goes forward until end of board or whitespace
        for (int i = row + 1; i < 15; i++) {
            if (board[(i * 15) + column] == 0)
                break;

            range.add(i);
        }

        return range;
    }

    private void updatePoints() {
        scoreView.setText(Integer.toString(points));
    }

    private String getHorizontalWord(int row, int column) {
        StringBuilder horizontal = new StringBuilder();

        for (Integer i : getHorizontalRange(row, column))
            horizontal.append(board[(row * 15) + i]);

        Log.d(TAG, "Horizontal word: " + horizontal.toString());

        // If it's just one character, don't have to check if word is valid
        if (horizontal.length() == 1)
            return null;

        return horizontal.toString();
    }

    private String getVerticalWord(int row, int column) {
        StringBuilder vertical = new StringBuilder();

        for (Integer i : getVerticalRange(row, column))
            vertical.append(board[(i * 15) + column]);

        Log.d(TAG, "Vertical word: " + vertical.toString());

        // If it's just one character, don't have to check if word is valid
        if (vertical.length() == 1)
            return null;

        return vertical.toString();
    }

    private boolean isValidWord(String word) {

        if (word == null)
            return true;
        else {
            final Cursor cursor = database.rawQuery("SELECT word FROM words WHERE word = '" + word.toLowerCase() + "'", null);
            if (cursor != null) {
                try {
                    if (cursor.moveToFirst()) {
                        Log.d(TAG, word + " is a valid word.");
                        return true;
                    }
                } finally {
                    cursor.close();
                }
            }

            Log.d(TAG, word + " is not a valid word.");
            return false;
        }
    }

    private int getPoints(char letter) {
        return Letter.valueOf(String.valueOf(letter)).getPoints();
    }

    private int getPointsHorizontalWord(int row, int column) {

        int points = 0;

        for (Integer i : getHorizontalRange(row, column)) {

            // Only add points, if letter was not part of a word before
            if (!isWord[(row * 15) + i]) {
                points = points + getPoints(board[(row * 15) + i]);
                isWord[(row * 15) + i] = true;
            }
        }

        return points;
    }

    private int getPointsVerticalWord(int row, int column) {

        int points = 0;

        for (Integer i : getVerticalRange(row, column)) {

            // Only add points, if letter was not part of a word before
            if (!isWord[(i * 15) + column]) {
                points = points + getPoints(board[(i * 15) + column]);
                isWord[(i * 15) + column] = true;
            }
        }

        return points;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                startActivity(new Intent(this, Prefs.class));
                return true;
            // More items go here (if any) ...
        }
        return false;
    }

    private void showGameOverDialog() {

        Log.d(TAG, "Game Over");

        // Clear current Game from SharedPreferences
        clearGame();

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(R.string.game_over)
                .setNegativeButton(R.string.quit, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        gameOverDialog.dismiss();
                        setResult(RESULT_CANCELED, null);
                        finish();
                    }
                })
                .setCancelable(false);

        gameOverDialog = builder.create();
        gameOverDialog.setCancelable(false);
        gameOverDialog.setCanceledOnTouchOutside(false);
        gameOverDialog.show();
    }

    private void timerWarning() {
        timerView.setTextColor(Color.RED);
        final Animation animation = new AlphaAnimation(1, 0); // Change alpha from fully visible to invisible
        animation.setDuration(1000);
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE);
        animation.setRepeatMode(Animation.REVERSE);
        timerView.startAnimation(animation);
    }

    private void cancelTimerWarning() {
        timerView.setTextColor(getResources().getColor(R.color.word_fade_yellow));
        timerView.clearAnimation();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {

        if (requestCode == EXIT) {
            // User pressed "Quit" on Pause menu
            if (resultCode == RESULT_OK) {
                saveGame();
                updateGameOnline(true);
                setResult(RESULT_OK);
                this.finish();
            }
            // User pressed "Resume Game" on Pause menu
            else if (resultCode == RESULT_CANCELED)
                resumeFading();
        }
    }

    public void pauseGame(View view) {
        Log.d(TAG, "Game Pause");
        stopFading();
        updateGameOnline(true);
        Intent intent = new Intent(this, Pause.class);
        startActivityForResult(intent, EXIT);
    }

    private void stopFading() {
        Log.d(TAG, "Stop Timer, Stop Fading");
        // Stop timer
        timerHandler.removeCallbacks(timerRunnable);
        // Stop letters from fading
        stopLettersFade();
        cancelTimerWarning();
    }

    private void resumeFading() {
        Log.d(TAG, "Resume Timer, Resume Fading");
        // Update Start Time so that the elapsed time is the same
        startTime = System.currentTimeMillis() - elapsedTime;
        // Restart timer
        timerHandler.postDelayed(timerRunnable, 0);
        startLettersFade(getRemainingTime());
    }

    private long getRemainingTime() {
        long remainingTime = 30000 - (System.currentTimeMillis() - startTime);

        if (remainingTime >= 0)
            return remainingTime;
        return 30000;
    }

    private void saveGame() {
        Log.d(TAG, "Save Game");
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putBoolean(CONTINUE, true);
        editor.putInt(POINTS, points);
        editor.putLong(ELAPSED_TIME, elapsedTime);
        editor.putString(BOARD, String.valueOf(board));
        saveIsWord();
        saveLetters();
        saveBunch();
        editor.commit();
    }

    private void clearGame() {
        Log.d(TAG, "Clear Game");
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            setResult(RESULT_OK, null);
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void saveIsWord() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(IS_WORD, true);
        for (int i = 0; i < isWord.length; i++)
            editor.putBoolean(IS_WORD + "_" + i, isWord[i]);
        editor.commit();
    }

    private boolean[] loadIsWord() {
        boolean array[] = new boolean[225];

        for (int i = 0; i < 225; i++)
            array[i] = sharedPreferences.getBoolean(IS_WORD + "_" + i, false);
        return array;
    }

    private void saveLetters() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(LETTERS, TextUtils.join(",", letters));
        editor.commit();
    }

    private void saveBunch() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(BUNCH, TextUtils.join(",", bunch));
        editor.commit();
    }

    private ArrayList<String> loadLetters() {
        String serialized = sharedPreferences.getString(LETTERS, null);
        ArrayList<String> array = new ArrayList<>(21);
        array.addAll(Arrays.asList(TextUtils.split(serialized, ",")));
        return array;
    }

    private ArrayList<String> loadBunch() {
        String serialized = sharedPreferences.getString(BUNCH, null);
        ArrayList<String> array = new ArrayList<>();
        array.addAll(Arrays.asList(TextUtils.split(serialized, ",")));
        return array;
    }

    private void drawBoard() {
        for (int i = 0; i < 15; i++) {
            for (int j = 0; j < 15; j++) {
                int viewID = getResources().getIdentifier("cell" + i + "_" + j, "id", getPackageName());
                ImageView tileView = (ImageView) findViewById(viewID);

                if (board[(i * 15) + j] != 0 && tileView != null) {
                    int drawableID = getResources().getIdentifier(String.valueOf(board[(i * 15) + j]).toLowerCase() + "_selector", "drawable", getPackageName());
                    tileView.setImageResource(drawableID);
                }
            }
        }
    }

    private void deselectLetter() {

        if (letterSelectedView != null)
            letterSelectedView.setAlpha(255);

        letterSelectedView = null;

        // Hides Dump button
        dumpButton.setVisibility(View.GONE);
    }

    private void showDumpButton() {
        if (letters.size() <= 18 && bunch.size() >= 3)
            dumpButton.setVisibility(View.VISIBLE);
    }

    private boolean hasWord() {
        for (int i = 0; i < isWord.length; i++)
            if (isWord[i] == true)
                return true;

        return false;
    }

    private boolean isConnected(int row, int column) {

        // The new word is connected to previous words if and only if
        // one of its letters already had the isWord flag set to true

        // Get letters on the horizontal range
        // Check if one of them has isWord set to true
        for (Integer i : getHorizontalRange(row, column))
            if (isWord[(row * 15) + i] == true)
                return true;

        // Get letters on the vertical range
        // Check if one of them has isWord set to true
        for (Integer i : getVerticalRange(row, column))
            if (isWord[(i * 15) + column])
                return true;

        return false;
    }

    private void validWordUpdate() {

        Log.d(TAG, "VALID WORD");
        startTime = System.currentTimeMillis();
        startLettersFade(getRemainingTime());
        MusicSound.play(this, R.raw.correct);
        updatePoints();
        cancelTimerWarning();
        timerHandler.postDelayed(timerRunnable, 0);

        // Update the game saved online when valid words are found
        updateGameOnline(false);
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    @SuppressLint("NewApi")
    public String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION,
                Integer.MIN_VALUE);
        Log.i(TAG, String.valueOf(registeredVersion));
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }

    private SharedPreferences getGCMPreferences(Context context) {
        return getSharedPreferences(Communication.class.getSimpleName(), Context.MODE_PRIVATE);
    }

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    private void getPlayerInfo() {
        String playerID = getIntent().getStringExtra(Constants.USER);

        if (playerID != null && !playerID.isEmpty()) {
            ParseQuery<ParseObject> query = ParseQuery.getQuery(Constants.USER);
            query.getInBackground(playerID, new GetCallback<ParseObject>() {
                public void done(ParseObject user, ParseException e) {
                    if (e == null) {
                        player = user;
                        playerName = user.getString(Constants.USER_NAME);

                        // If we're starting a new game, we save it online
                        if (getIntent().getBooleanExtra(NEW_GAME, false))
                            saveGameOnline();

                        // If not, we're resuming a game, so we retrieve its ID
                        else
                            getGameInfo();

                        // Get the opponent's registration id for sending notifications
                        getOpponentInfo();

                    } else {
                        Log.d(TAG, "Error: " + e.getMessage());
                    }
                }
            });
        }
    }

    private void getMatchInfo() {
        String matchID = getIntent().getStringExtra(Constants.MATCH);

        if (matchID != null && !matchID.isEmpty()) {
            ParseQuery<ParseObject> query = ParseQuery.getQuery(Constants.MATCH);
            query.getInBackground(matchID, new GetCallback<ParseObject>() {
                public void done(ParseObject object, ParseException e) {
                    if (e == null) {
                        match = object;
                        getPlayerInfo();
                    } else {
                        Log.d(TAG, "Error: " + e.getMessage());
                    }
                }
            });
        }
    }

    private void getOpponentInfo() {
        String opponentID = getIntent().getStringExtra(Constants.OPPONENT);

        if (opponentID != null && !opponentID.isEmpty()) {
            ParseQuery<ParseObject> query = ParseQuery.getQuery(Constants.USER);
            query.getInBackground(opponentID, new GetCallback<ParseObject>() {
                public void done(ParseObject opponent, ParseException e) {
                    if (e == null) {
                        opponentRegistrationID = opponent.getString(Constants.REGISTRATION_ID);
                    } else {
                        Log.d(TAG, "Error: " + e.getMessage());
                    }
                }
            });
        }
    }

    private void getGameInfo() {
        String gameID = getIntent().getStringExtra(Constants.GAME);

        if (gameID != null && !gameID.isEmpty()) {

            ParseQuery<ParseObject> query = ParseQuery.getQuery(Constants.GAME);
            query.getInBackground(gameID, new GetCallback<ParseObject>() {
                public void done(ParseObject object, ParseException e) {
                    if (e == null) {
                        game = object;
                    } else {
                        Log.d(TAG, "Error: " + e.getMessage());
                    }
                }
            });
        }
    }

    private void saveScore(int score) {
        if (playerName != null && !playerName.isEmpty()) {
            ParseObject highScore = new ParseObject(Constants.HIGH_SCORE);
            highScore.put(Constants.USER_NAME, playerName);
            highScore.put(Constants.SCORE, score);
            highScore.saveInBackground();

            sendMessage( "Game Over! " + playerName + " has scored " + score + " points!");

            // If player one has played, we let player two know it's his turn
            if(getIntent().getIntExtra(Constants.PLAYER, 0) == 1)
                sendMessage( "It's your turn!");
        }
    }

    private void saveGameOnline() {
        if (player != null) {
            ParseObject game = new ParseObject(Constants.GAME);
            game.put(Constants.USER, player);
            game.put(Constants.BUNCH, TextUtils.join(",", bunch));
            game.put(Constants.RACK, TextUtils.join(",", letters));
            game.put(Constants.OVER, false);
            game.saveInBackground();
            this.game = game;
            Log.d(TAG, "Saved Game online");

            sendMessage(playerName + " has started a Word Fade game against you!");

            if(getIntent().getIntExtra(Constants.PLAYER, 0) == 1)
                match.put(Constants.GAME_ONE, game);
            else if (getIntent().getIntExtra(Constants.PLAYER, 0) == 2)
                match.put(Constants.GAME_TWO, game);

            match.saveInBackground();
        }
    }

    private void updateGameOnline(final boolean pause) {
        if (game != null) {
            game.put(Constants.BUNCH, TextUtils.join(",", bunch));
            game.put(Constants.RACK, TextUtils.join(",", letters));
            game.put(Constants.SCORE, points);
            game.put(Constants.ELAPSED_TIME, elapsedTime);
            game.put(Constants.BOARD, String.valueOf(board));
            game.put(Constants.IS_WORD, gson.toJson(isWord));
            game.put(Constants.PAUSE, pause);
            game.put(Constants.OVER, over);
            game.saveInBackground();
            Log.d(TAG, "Updated Game online");
        }
    }

    @SuppressLint("NewApi")
    private void sendMessage(final String message) {
        if (registrationID == null || registrationID.equals("")) {
            Toast.makeText(this, "You must register first", Toast.LENGTH_LONG).show();
            return;
        }
        if (message.isEmpty()) {
            Toast.makeText(this, "Empty Message", Toast.LENGTH_LONG).show();
            return;
        }

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                List<String> regIds = new ArrayList<>();
                String reg_device = opponentRegistrationID;
                int nIcon = R.drawable.ic_stat_cloud;
                int nType = CommunicationConstants.SIMPLE_NOTIFICATION;
                Map<String, String> msgParams;
                msgParams = new HashMap<>();
                msgParams.put("data.alertText", "Word Fade Match");
                msgParams.put("data.titleText", "Word Fade");
                msgParams.put("data.contentText", message);
                msgParams.put("data.nIcon", String.valueOf(nIcon));
                msgParams.put("data.nType", String.valueOf(nType));
                GCMNotification gcmNotification = new GCMNotification();
                regIds.clear();
                regIds.add(reg_device);
                gcmNotification.sendNotification(msgParams, regIds, TwoPlayerGame.this);

                return null;
            }
        }.execute(null, null, null);
    }
}
