package edu.neu.madcourse.analuizamouradacunha;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import edu.neu.madcourse.analuizamouradacunha.communication.Communication;
import edu.neu.madcourse.analuizamouradacunha.dictionary.Dictionary;
import edu.neu.madcourse.analuizamouradacunha.foodcrunch.DescriptionActivity;
import edu.neu.madcourse.analuizamouradacunha.sudoku.Sudoku;
import edu.neu.madcourse.analuizamouradacunha.trickiestpart.TrickiestPart;
import edu.neu.madcourse.analuizamouradacunha.twoplayerwordgame.TwoPlayerWordGame;
import edu.neu.madcourse.analuizamouradacunha.wordgame.WordGame;

public class Main extends Activity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Removes title bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.main);

        Button mAboutButton = (Button) findViewById(R.id.about_button);
        mAboutButton.setOnClickListener(this);

        Button mErrorButton = (Button) findViewById(R.id.error_button);
        mErrorButton.setOnClickListener(this);

        Button mSudokuButton = (Button) findViewById(R.id.sudoku_button);
        mSudokuButton.setOnClickListener(this);

        Button mDictionaryButton = (Button) findViewById(R.id.dictionary_button);
        mDictionaryButton.setOnClickListener(this);

        Button mWordGameButton = (Button) findViewById(R.id.word_game_button);
        mWordGameButton.setOnClickListener(this);

        Button mCommunicationButton = (Button) findViewById(R.id.communication_button);
        mCommunicationButton.setOnClickListener(this);

        Button mTwoPlayerWordGameButton = (Button) findViewById(R.id.two_player_word_game_button);
        mTwoPlayerWordGameButton.setOnClickListener(this);

        Button mTrickiestPartButton = (Button) findViewById(R.id.trickiest_part_button);
        mTrickiestPartButton.setOnClickListener(this);

        Button mFinalProjectButton = (Button) findViewById(R.id.final_project_button);
        mFinalProjectButton.setOnClickListener(this);

        Button mQuitButton = (Button) findViewById(R.id.quit_button);
        mQuitButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.error_button:
                generateError();
                break;
            case R.id.about_button:
                intent = new Intent(this, About.class);
                startActivity(intent);
                break;
            case R.id.sudoku_button:
                intent = new Intent(this, Sudoku.class);
                startActivity(intent);
                break;
            case R.id.acknowledgements_button:
                intent = new Intent(this, Acknowledgements.class);
                startActivity(intent);
                break;
            case R.id.dictionary_button:
                intent = new Intent(this, Dictionary.class);
                startActivity(intent);
                break;
            case R.id.word_game_button:
                intent = new Intent(this, WordGame.class);
                startActivity(intent);
                break;
            case R.id.communication_button:
                intent = new Intent(this, Communication.class);
                startActivity(intent);
                break;
            case R.id.two_player_word_game_button:
                intent = new Intent(this, TwoPlayerWordGame.class);
                startActivity(intent);
                break;
            case R.id.trickiest_part_button:
                intent = new Intent(this, TrickiestPart.class);
                startActivity(intent);
                break;
            case R.id.final_project_button:
                intent = new Intent(this, DescriptionActivity.class);
                startActivity(intent);
                break;
            case R.id.quit_button:
                finish();
                break;
        }
    }

    private void generateError() {
        throw new RuntimeException("Generate Error button");
    }
}
