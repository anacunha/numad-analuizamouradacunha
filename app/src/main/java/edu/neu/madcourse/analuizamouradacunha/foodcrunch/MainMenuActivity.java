package edu.neu.madcourse.analuizamouradacunha.foodcrunch;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.parse.CountCallback;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

import edu.neu.madcourse.analuizamouradacunha.R;

public class MainMenuActivity extends Activity implements View.OnClickListener {

    private String mUserPhoneNumber;
    private static final String COMMENT = "COMMENT";
    private static final String FOOD_CRUNCH_SHARED_PREF = "FOOD_CRUNCH_SHARED_PREF";
    private static final String LIKE = "LIKE";
    private static final String NUMBER = "number";
    private static final String PHONE_NUMBER = "PHONE_NUMBER";
    private static final String PHONE_NUMBER_DB = "PHONE_NUMBER_DB";
    private static final String PHOTO = "PHOTO";
    private static final String TAG = "Food Crunch";
    private static final ArrayList<String> contactsList = new ArrayList<>();
    private static final ArrayList<String> parsePhoneNumbers = new ArrayList<>();
    private Context context;
    private int totalLikes;
    public static SharedPreferences mPref;
    public static SharedPreferences.Editor mEditor;
    public static final ArrayList<String> filteredPhoneNumbers = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.food_crunch_main_menu);
        setTitle("Food Crunch");

        context = getApplicationContext();

        Button mMyBoardButton = (Button) findViewById(R.id.my_board);
        mMyBoardButton.setOnClickListener(this);
        Button mFriendsBoardButton = (Button) findViewById(R.id.friends_board);
        mFriendsBoardButton.setOnClickListener(this);
        Button mAchievementsButton = (Button) findViewById(R.id.achievements);
        mAchievementsButton.setOnClickListener(this);
        Button mLeaderboardsButton = (Button) findViewById(R.id.leaderboards);
        mLeaderboardsButton.setOnClickListener(this);
        Button mOptionsButton = (Button) findViewById(R.id.options);
        mOptionsButton.setOnClickListener(this);

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        mUserPhoneNumber = telephonyManager.getLine1Number();
        Log.d(TAG, "User Phone Number: " + mUserPhoneNumber);

        // For users without a valid phone number
        if(mUserPhoneNumber == null) {
            Toast.makeText(context,
                    "We're sorry! Food Crunch is only available for users with a valid phone number.",
                    Toast.LENGTH_LONG).show();
            finish();
        }

        if (cm.getActiveNetworkInfo() != null &&
                cm.getActiveNetworkInfo().isAvailable() &&
                cm.getActiveNetworkInfo().isConnected()) {
            checkPhoneNumber();
        } else {
            Toast.makeText(context, "Internet connection failed", Toast.LENGTH_LONG).show();
            finish();
        }

        totalLikes = 0;
    }

    @Override
    public void onResume() {
        super.onResume();
        checkForAchievements();
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.my_board:
                intent = new Intent(this, MyBoardActivity.class);
                startActivity(intent);
                break;
            case R.id.friends_board:
                FriendsBoardDialog friendsBoardDialog = new FriendsBoardDialog(MainMenuActivity.this);
                friendsBoardDialog.show();
                break;
            case R.id.achievements:
                intent = new Intent(this, AchievementsActivity.class);
                startActivity(intent);
                break;
            case R.id.leaderboards:
                intent = new Intent(this, LeaderboardsActivity.class);
                startActivity(intent);
                break;
            case R.id.options:
                intent = new Intent(this, OptionsActivity.class);
                startActivity(intent);
                break;
        }
    }

    private void checkPhoneNumber() {
        if (mUserPhoneNumber != null) {
            ParseQuery<ParseObject> query = ParseQuery.getQuery(PHONE_NUMBER_DB);
            query.whereEqualTo(NUMBER, mUserPhoneNumber);
            query.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List<ParseObject> parseObjects, ParseException e) {
                    if (e == null) {
                        if (parseObjects.size() == 0) {
                            putUserPhoneNumberInParse();
                            Log.d(TAG, "Registered Phone Number: " + mUserPhoneNumber);
                        } else
                            Log.d(TAG, "Phone Number " + mUserPhoneNumber + " already registered.");

                        getFilteredContactsPhoneNumbers();
                    } else {
                        Log.d(TAG, "Error: " + e.getMessage());
                    }
                }
            });
        }
    }

    public void putUserPhoneNumberInParse() {
        ParseObject userPhoneNumberObject = new ParseObject(PHONE_NUMBER_DB);
        userPhoneNumberObject.put(NUMBER, mUserPhoneNumber);
        userPhoneNumberObject.saveInBackground();
    }

    public void getFilteredContactsPhoneNumbers() {
        try {
            final Cursor c = context.getContentResolver().query(
                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null,
                    ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER + " ASC");

            while(c.moveToNext()) {
                String phoneNumber = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER));

                if (phoneNumber != null
                        && !contactsList.contains(phoneNumber)
                        && !phoneNumber.equals(mUserPhoneNumber))
                    contactsList.add(phoneNumber);
            }

            c.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }

        for (int i = 0; i < contactsList.size(); i++)
            if (contactsList.get(i).startsWith("+"))
                contactsList.set(i, contactsList.get(i).substring(1));

        getParsePhoneNumbers();
    }

    public void getParsePhoneNumbers() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery(PHONE_NUMBER_DB);
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> phoneNumbersList, ParseException e) {
                if (e == null) {
                    for (ParseObject parseObject : phoneNumbersList) {
                        String number = parseObject.getString(NUMBER);
                        if (!parsePhoneNumbers.contains(number)) {
                            parsePhoneNumbers.add(number);
                        }
                    }
                    crossMatchNumbers();

                } else {
                    Log.d(TAG, "Error: " + e.getMessage());
                }
            }
        });
    }

    public void crossMatchNumbers() {
        Log.d(TAG, "Parse Phone Numbers: " + parsePhoneNumbers.toString());

        for (String number : parsePhoneNumbers)
            if (contactsList.contains(number)
                    && !number.equals(mUserPhoneNumber)
                    && !filteredPhoneNumbers.contains("+" + number))
                filteredPhoneNumbers.add("+" + number);

        Log.d(TAG, "Filtered Phone Numbers: " + filteredPhoneNumbers.toString());
    }

    private void getLikesCount() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery(PHOTO);
        query.whereEqualTo(PHONE_NUMBER, mUserPhoneNumber);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> photos, ParseException e) {
                for(ParseObject photo : photos) {

                    ParseQuery<ParseObject> query = ParseQuery.getQuery(LIKE);
                    query.whereEqualTo(PHOTO, photo);
                    query.countInBackground(new CountCallback() {
                        @Override
                        public void done(int count, ParseException e) {
                            increaseTotalLikes(count);

                            if (totalLikes >= 10) {
                                mEditor.putBoolean("achievement3", true);
                                mEditor.apply();
                                Toast toast = Toast.makeText(getApplicationContext(),
                                        "Achievement Unlocked:\n Got 10 Likes!", Toast.LENGTH_LONG);
                                toast.show();
                                return;
                            }
                        }
                    });
                }
            }
        });
    }

    private void increaseTotalLikes(int amount) {
        totalLikes = totalLikes + amount;
        // Log.d(TAG, "User has " + totalLikes + " likes total");
    }

    private void getCommentsCount() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery(PHOTO);
        query.whereEqualTo(PHONE_NUMBER, mUserPhoneNumber);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> photos, ParseException e) {
                for(ParseObject photo : photos) {

                    ParseQuery<ParseObject> query = ParseQuery.getQuery(COMMENT);
                    query.whereEqualTo(PHOTO, photo);
                    query.countInBackground(new CountCallback() {
                        @Override
                        public void done(int count, ParseException e) {
                            if (count >= 1) {
                                mEditor.putBoolean("achievement4", true);
                                mEditor.apply();
                                Toast toast = Toast.makeText(getApplicationContext(),
                                        "Achievement Unlocked:\n Your First Comment!", Toast.LENGTH_LONG);
                                toast.show();
                                return;
                            }
                        }
                    });
                }
            }
        });
    }

    private void checkForAchievements() {
        mPref = getSharedPreferences(FOOD_CRUNCH_SHARED_PREF, MODE_PRIVATE);
        mEditor = mPref.edit();

        if (!mPref.getBoolean("achievement0", false)){
            //if achievement1 is false (the achievement for downloading the app)
            //then give them the achievement and show the toast
            mEditor.putBoolean("achievement0", true);
            mEditor.apply();
            Toast toast = Toast.makeText(context, "Achievement Unlocked:\n Got FoodCrunch!", Toast.LENGTH_LONG);
            toast.show();
        }

        // Only check for achievements if the user doesn't have them yet
        if (!mPref.getBoolean("achievement3", false))
            getLikesCount();

        if (!mPref.getBoolean("achievement4", false))
            getCommentsCount();
    }
}
