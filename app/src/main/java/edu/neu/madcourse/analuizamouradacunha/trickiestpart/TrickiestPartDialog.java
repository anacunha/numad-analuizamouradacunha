package edu.neu.madcourse.analuizamouradacunha.trickiestpart;

import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import edu.neu.madcourse.analuizamouradacunha.R;

public class TrickiestPartDialog extends Dialog {

    private Context context;
    private ArrayList<String> contactsList = new ArrayList<>();

    public TrickiestPartDialog(Context context){
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Removes title bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.trickiest_part_dialog);

        try{
            final Cursor c = context.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);

            while(c.moveToNext()) {
                String contactName = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if (contactName != null)
                    contactsList.add(contactName);
            }

            c.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }

        populateListView();
        registerClickCallback();
    }

    private void populateListView() {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(context, R.layout.array_adapter_styling, contactsList);

        ListView list = (ListView)findViewById(R.id.listViewMain);

        list.setAdapter(adapter);
    }

    private void registerClickCallback() {
        ListView list = (ListView)findViewById(R.id.listViewMain);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View viewClicked, int position, long id) {
                TextView textView = (TextView) viewClicked;
                //Do toast or something...
            }
        });
    }
}
