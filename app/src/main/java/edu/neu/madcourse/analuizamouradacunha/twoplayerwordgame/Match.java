package edu.neu.madcourse.analuizamouradacunha.twoplayerwordgame;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.neu.madcourse.analuizamouradacunha.R;
import edu.neu.madcourse.analuizamouradacunha.communication.Communication;
import edu.neu.madcourse.analuizamouradacunha.communication.CommunicationConstants;
import edu.neu.madcourse.analuizamouradacunha.communication.Constants;
import edu.neu.madcourse.analuizamouradacunha.communication.GCMNotification;

public class Match extends Activity implements View.OnClickListener {

    private static final String TAG = "Match";
    private static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private static final String NEW_GAME = "NEW_GAME";
    private static final String GAME_ID = "GAME_ID";
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private String registrationID;
    private String recipientRegistrationID;
    private String senderUserName;
    private Context context;
    private Button button1;
    private Button abandonMatchButton;
    private TextView turnTextView;
    private TextView yourScoreView;
    private TextView opponentScoreView;
    private boolean host;
    private ParseObject playerOne;
    private ParseObject playerTwo;
    private ParseObject match;
    private ParseObject gameOne;
    private ParseObject gameTwo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Removes title bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.match);

        turnTextView = (TextView) findViewById(R.id.turn);
        yourScoreView = (TextView) findViewById(R.id.your_score);
        opponentScoreView = (TextView) findViewById(R.id.opponents_score);

        button1 = (Button) findViewById(R.id.button1);
        button1.setOnClickListener(this);

        abandonMatchButton = (Button) findViewById(R.id.abandon_match_button);
        abandonMatchButton.setOnClickListener(this);

        Button quitButton = (Button) findViewById(R.id.quit_button);
        quitButton.setOnClickListener(this);

        context = getApplicationContext();
    }

    @Override
    public void onResume() {
        super.onResume();

        Log.d(TAG, "onResume");
        getPlayerOne();
    }

    private void updateUI() {
        TextView title = (TextView) findViewById(R.id.matchTitle);
        title.setText(playerOne.getString(Constants.USER_NAME) + " vs. " + playerTwo.getString(Constants.USER_NAME));
    }

    private void getPlayerOne() {
        String playerOneID = getIntent().getStringExtra(Constants.PLAYER_ONE);

        if(playerOneID != null && !playerOneID.isEmpty()) {
            ParseQuery<ParseObject> query = ParseQuery.getQuery(Constants.USER);
            query.getInBackground(playerOneID, new GetCallback<ParseObject>() {
                public void done(ParseObject object, ParseException e) {
                    if (e == null) {
                        playerOne = object;
                        getPlayerTwo();
                    } else {
                        Toast.makeText(context, "Couldn't retrieve player one.", Toast.LENGTH_LONG).show();
                        finish();
                    }
                }
            });
        }
    }

    private void getPlayerTwo() {
        String playerTwoID = getIntent().getStringExtra(Constants.PLAYER_TWO);

        if(playerTwoID != null && !playerTwoID.isEmpty()) {
            ParseQuery<ParseObject> query = ParseQuery.getQuery(Constants.USER);
            query.getInBackground(playerTwoID, new GetCallback<ParseObject>() {
                public void done(ParseObject object, ParseException e) {
                    if (e == null) {
                        playerTwo = object;
                        getRegistration();
                    } else {
                        Toast.makeText(context, "Couldn't retrieve player two.", Toast.LENGTH_LONG).show();
                        finish();
                    }
                }
            });
        }
    }

    private void getMatch() {
        String matchID = getIntent().getStringExtra(Constants.MATCH);

        // If there's no match id, a new match must be created
        if (getIntent().getBooleanExtra(Constants.NEW_MATCH, false)) {
            getIntent().removeExtra(Constants.NEW_MATCH);
            createMatch();
            sendMessage(senderUserName + " has challenged you for a WordFade match.");
        }
        else {
            ParseQuery<ParseObject> query = ParseQuery.getQuery(Constants.MATCH);
            query.getInBackground(matchID, new GetCallback<ParseObject>() {
                public void done(ParseObject object, ParseException e) {
                    if (e == null) {
                        match = object;
                        try {
                            gameOne = match.getParseObject(Constants.GAME_ONE);
                            gameTwo = match.getParseObject(Constants.GAME_TWO);

                            if(gameOne != null)
                                gameOne.fetchIfNeeded();
                            if(gameTwo != null)
                                gameTwo.fetchIfNeeded();

                            checkGameStatus();
                        }
                        catch (ParseException ex) {
                            ex.printStackTrace();
                        }
                    } else {
                        Toast.makeText(context, "Couldn't retrieve match information", Toast.LENGTH_LONG).show();
                        finish();
                    }
                }
            });
        }
    }

    @SuppressLint("NewApi")
    private void sendMessage(final String message) {
        if (registrationID == null || registrationID.equals("")) {
            Toast.makeText(this, "You must register first", Toast.LENGTH_LONG).show();
            return;
        }
        if (message.isEmpty()) {
            Toast.makeText(this, "Empty Message", Toast.LENGTH_LONG).show();
            return;
        }

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                List<String> regIds = new ArrayList<>();
                String reg_device = recipientRegistrationID;
                int nIcon = R.drawable.ic_stat_cloud;
                int nType = CommunicationConstants.SIMPLE_NOTIFICATION;
                Map<String, String> msgParams;
                msgParams = new HashMap<>();
                msgParams.put("data.alertText", "Word Fade Match");
                msgParams.put("data.titleText", "Word Fade");
                msgParams.put("data.contentText", message);
                msgParams.put("data.nIcon", String.valueOf(nIcon));
                msgParams.put("data.nType", String.valueOf(nType));
                GCMNotification gcmNotification = new GCMNotification();
                regIds.clear();
                regIds.add(reg_device);
                gcmNotification.sendNotification(msgParams, regIds, Match.this);

                return null;
            }
        }.execute(null, null, null);
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    private void getRegistration() {
        if (checkPlayServices()) {
            registrationID = getRegistrationId(context);

            // Current user is the player one
            if (playerOne.getString(Constants.REGISTRATION_ID).equals(registrationID)) {
                host = true;

                // Player two will be the recipient of notifications
                recipientRegistrationID = playerTwo.getString(Constants.REGISTRATION_ID);
                // Player one will be the sender
                senderUserName = playerOne.getString(Constants.USER_NAME);
            }
            // Current user is the player two
            else {
                // Player one will be the recipient of notifications
                recipientRegistrationID = playerOne.getString(Constants.REGISTRATION_ID);
                // Player two will be the sends
                senderUserName = playerTwo.getString(Constants.USER_NAME);
            }

            getMatch();
            updateUI();
        }
    }

    @SuppressLint("NewApi")
    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION,Integer.MIN_VALUE);
        Log.i(TAG, String.valueOf(registeredVersion));
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }

    private SharedPreferences getGCMPreferences(Context context) {
        return getSharedPreferences(Communication.class.getSimpleName(), Context.MODE_PRIVATE);
    }

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    @Override
    public void onClick(View v) {

        Intent intent;

        switch (v.getId()) {
            case R.id.button1:
                intent = new Intent(this, TwoPlayerGame.class);
                intent.putExtra(Constants.MATCH, match.getObjectId());

                if(host) {
                    intent.putExtra(Constants.USER, playerOne.getObjectId());
                    intent.putExtra(Constants.OPPONENT, playerTwo.getObjectId());
                    intent.putExtra(Constants.PLAYER, 1);
                }
                else {
                    intent.putExtra(Constants.USER, playerTwo.getObjectId());
                    intent.putExtra(Constants.OPPONENT, playerOne.getObjectId());
                    intent.putExtra(Constants.PLAYER, 2);
                }

                // New Game
                if(button1.getText().equals("Play")) {
                    intent.putExtra(NEW_GAME, true);
                }
                // Resume Game
                else if (button1.getText().equals("Resume")) {
                    if (host)
                        intent.putExtra(GAME_ID, match.getParseObject(Constants.GAME_ONE).getObjectId());
                    else
                        intent.putExtra(GAME_ID, match.getParseObject(Constants.GAME_TWO).getObjectId());
                }

                startActivity(intent);
                break;

            case R.id.abandon_match_button:
                deleteMatch();
                Toast.makeText(this, "Abandoned match!", Toast.LENGTH_LONG).show();
                setResult(RESULT_CANCELED);

            case R.id.quit_button:
                intent = new Intent(this, TwoPlayerWordGame.class);
                startActivity(intent);
                finish();
                break;
        }
    }

    private void createMatch() {
        if(playerOne != null && playerTwo != null) {
            match = new ParseObject(Constants.MATCH);
            match.put(Constants.PLAYER_ONE, playerOne);
            match.put(Constants.PLAYER_TWO, playerTwo);
            match.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null)
                        getIntent().putExtra(Constants.MATCH, match.getObjectId());
                }
            });
        }
    }

    private void deleteMatch() {
        if(match != null) {
            match.deleteInBackground();
            sendMessage(senderUserName + " has abandoned your match.");
        }
    }

    private void checkGameStatus() {

        Log.d(TAG, "checkGameStatus()");
        // Player one
        if (host) {
            // Hasn't started game yet
            if (gameOne == null) {
                button1.setText("Play");
            }
            // Has started the game
            else {

                // Game is not over
                if (!gameOne.getBoolean(Constants.OVER)) {
                    button1.setText("Resume");
                }
                // Game is over
                else {

                    // If opponent's game is not over
                    if (gameTwo == null || !gameTwo.getBoolean(Constants.OVER)) {
                        yourScoreView.setText("Your score: " + gameOne.getInt(Constants.SCORE));
                        turnTextView.setText("Opponent's turn");
                        button1.setVisibility(View.GONE);
                    }
                    // If opponent's game is over
                    else if (gameTwo != null && gameTwo.getBoolean(Constants.OVER)) {
                        yourScoreView.setText("Your score: " + gameOne.getInt(Constants.SCORE));
                        opponentScoreView.setText("Opponent's score: " + gameTwo.getInt(Constants.SCORE));
                        button1.setVisibility(View.GONE);
                        abandonMatchButton.setVisibility(View.GONE);
                        getWinner(gameOne, gameTwo);
                    }
                }
            }
        }
        // Player two
        else {
            // Player one hasn't started or finished his game yet
            if (gameOne == null || !gameOne.getBoolean(Constants.OVER)) {
                button1.setVisibility(View.GONE);
                turnTextView.setText("Opponent's turn");
            }
            // Player one game is over
            else if (gameOne != null && gameOne.getBoolean(Constants.OVER)) {
                button1.setVisibility(View.VISIBLE);
                opponentScoreView.setText("Opponent's score: " + gameOne.getInt(Constants.SCORE));

                // Hasn't started game
                if (gameTwo == null) {
                    button1.setText("Play");
                }
                // Has started the game
                else {
                    // Game is not over
                    if (!gameTwo.getBoolean(Constants.OVER)) {
                        button1.setText("Resume");
                    }
                    // Game is over, Match is over
                    else {
                        yourScoreView.setText("Your score: " + gameTwo.getInt(Constants.SCORE));
                        opponentScoreView.setText("Opponent's score: " + gameOne.getInt(Constants.SCORE));
                        button1.setVisibility(View.GONE);
                        abandonMatchButton.setVisibility(View.GONE);
                        getWinner(gameOne, gameTwo);
                    }
                }
            }
        }
    }

    private void getWinner(ParseObject gameOne, ParseObject gameTwo) {
        ParseObject winner = match.getParseObject(Constants.WINNER);
        if (winner == null) {
            // Calculate winner
            int scoreOne = gameOne.getInt(Constants.SCORE);
            int scoreTwo = gameTwo.getInt(Constants.SCORE);
            if (scoreOne >= scoreTwo) {
                winner = playerOne;

                if (host)
                    sendMessage("You lost the match against " + senderUserName + "!");
            }
            else {
                winner = playerTwo;

                if (!host)
                    sendMessage("You lost the match against " + senderUserName + "!");
            }

            match.put(Constants.WINNER, winner);
            match.saveInBackground();
        }

        if (host && winner == playerOne)
            turnTextView.setText("You win!");
        else if (host && winner == playerTwo)
            turnTextView.setText("You lose!");
        else if (!host && winner == playerOne)
            turnTextView.setText("You lose!");
        else if (!host && winner == playerTwo)
            turnTextView.setText("You win!");
    }
}