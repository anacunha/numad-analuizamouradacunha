package edu.neu.madcourse.analuizamouradacunha.communication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import edu.neu.madcourse.analuizamouradacunha.R;
import edu.neu.madcourse.analuizamouradacunha.wordgame.WordGame;

public class Communication extends Activity implements OnClickListener {

    private Button mSinglePlayerGame;
    private Button mRegisterDevice;
    private Button mViewUsers;
    private Button mHighScores;
    private Button mAcknowledgements;
    private Button mQuit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.communication);

        mSinglePlayerGame = (Button) findViewById(R.id.single_player_game);
        mRegisterDevice = (Button) findViewById(R.id.register_device);
        mViewUsers = (Button) findViewById(R.id.view_users);
        mHighScores = (Button) findViewById(R.id.high_scores);
        mAcknowledgements = (Button) findViewById(R.id.acknowledgements);
        mQuit = (Button) findViewById(R.id.quit);

        mSinglePlayerGame.setOnClickListener(this);
        mRegisterDevice.setOnClickListener(this);
        mViewUsers.setOnClickListener(this);
        mHighScores.setOnClickListener(this);
        mAcknowledgements.setOnClickListener(this);
        mQuit.setOnClickListener(this);
    }

    @Override
    public void onClick(final View view) {

        Intent intent;

        switch (view.getId()) {
            case R.id.single_player_game:
                intent = new Intent(this, WordGame.class);
                startActivity(intent);
                break;

            case R.id.register_device:
                intent = new Intent(this, Registration.class);
                startActivity(intent);
                break;

            case R.id.view_users:
                intent = new Intent(this, Users.class);
                startActivity(intent);
                break;

            case R.id.high_scores:
                intent = new Intent(this, HighScores.class);
                startActivity(intent);
                break;

            case R.id.acknowledgements:
                intent = new Intent(this, Acknowledgements.class);
                startActivity(intent);
                break;

            case R.id.quit:
                finish();
                break;
        }
    }
}
