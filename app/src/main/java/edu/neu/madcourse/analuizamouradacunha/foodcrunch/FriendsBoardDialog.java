package edu.neu.madcourse.analuizamouradacunha.foodcrunch;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import edu.neu.madcourse.analuizamouradacunha.R;

public class FriendsBoardDialog extends Dialog {

    private static final String FRIEND_NAME = "FRIEND_NAME";
    private static final String FRIEND_PHONE_NUMBER = "FRIEND_PHONE_NUMBER";
    private static final String TAG = "Food Crunch";
    private Set<Contact> contactsList;
    private Context context;

    public FriendsBoardDialog(Context context){
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Removes title bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.trickiest_part_dialog);

        contactsList = new HashSet<>();

        try {
            final Cursor c = context.getContentResolver().query(
                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null, null,
                    ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " DESC");

            while(c.moveToNext()) {
                String contactName = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                String contactPhoneNumber = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER));
                Contact contact = new Contact(contactName, contactPhoneNumber);

                if (MainMenuActivity.filteredPhoneNumbers.contains(contactPhoneNumber))
                    contactsList.add(contact);
            }
            c.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        populateListView();
        registerClickCallback();
    }

    private void populateListView() {
        ContactAdapter adapter = new ContactAdapter(context, new ArrayList<>(contactsList));
        ListView list = (ListView) findViewById(R.id.listViewMain);
        list.setAdapter(adapter);
    }

    private void registerClickCallback() {
        ListView list = (ListView)findViewById(R.id.listViewMain);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View viewClicked, int position, long id) {
                LinearLayout linearLayout = (LinearLayout) viewClicked;
                TextView nameTextView = (TextView) linearLayout.getChildAt(0);
                TextView phoneNumberTextView = (TextView) linearLayout.getChildAt(1);

                String friendName = nameTextView.getText().toString();
                String friendPhoneNumber = phoneNumberTextView.getText().toString();

                Log.d(TAG, "Selected Contact: " + friendName);
                Log.d(TAG, "Selected Phone Number: " + friendPhoneNumber);

                Intent intent = new Intent(context, FriendsBoardActivity.class);
                intent.putExtra(FRIEND_NAME, friendName);

                if(friendPhoneNumber.startsWith("+"))
                    friendPhoneNumber = friendPhoneNumber.substring(1);
                intent.putExtra(FRIEND_PHONE_NUMBER, friendPhoneNumber);
                context.startActivity(intent);
            }
        });
    }
}
